package com.example.spirala1.util;

import android.app.IntentService;
import android.content.Intent;

import androidx.annotation.Nullable;

public class TransactionIntentService extends IntentService {
    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public TransactionIntentService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if(intent.hasExtra("GET")){

        }
        else if(intent.hasExtra("ADD")){

        }
        else if(intent.hasExtra("DELETE")){

        }
        else if(intent.hasExtra("FILTER")){
            String query = "";
            if(intent.hasExtra("year")&&intent.hasExtra("month")){
                query += "month="+intent.getStringExtra("month")+"&year="+intent.getStringExtra("year");
            }
            if(intent.hasExtra("type")){
            }
        }
        else if(intent.hasExtra("UPDATE")){

        }
    }
}
