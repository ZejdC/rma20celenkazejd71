package com.example.spirala1.util;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.spirala1.R;
import com.example.spirala1.data.Payment;
import com.example.spirala1.data.Transaction;

import java.util.List;

public class MyArrayAdapter extends ArrayAdapter<Transaction> {
    int res;
    public MyArrayAdapter(@NonNull Context context, int resource) {
        super(context, resource);
        res = resource;
    }


    public MyArrayAdapter(@NonNull Context context, int resource, int textViewResourceId, @NonNull List<Transaction> objects) {
        super(context, resource, textViewResourceId, objects);
        res = resource;
    }



    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LinearLayout newView;
        if(convertView == null){
            newView = new LinearLayout(getContext());
            String inflater = Context.LAYOUT_INFLATER_SERVICE;
            LayoutInflater li;
            li = (LayoutInflater)getContext().getSystemService(inflater);
            li.inflate(res, newView, true);
        }
        else{
            newView = (LinearLayout)convertView;
        }
        Transaction t = getItem(position);
        ImageView slika = (ImageView) newView.findViewById(R.id.slika);
        TextView title = (TextView) newView.findViewById(R.id.naziv);
        TextView amount = (TextView) newView.findViewById(R.id.iznos);
        title.setText(t.getTitle());
        amount.setText(t.getAmount()+"");
        if(t.getType().equals(Payment.REGULARPAYMENT)) {
            slika.setImageResource(R.drawable.regularpayment);
        }else if(t.getType().equals(Payment.INDIVIDUALPAYMENT)){
            slika.setImageResource(R.drawable.onetimepayment);
        }else if(t.getType().equals(Payment.PURCHASE)){
            slika.setImageResource(R.drawable.purchase);
        }else if(t.getType().equals(Payment.INDIVIDUALINCOME)){
            slika.setImageResource(R.drawable.individualincome);
        }else if(t.getType().equals(Payment.REGULARINCOME)){
            slika.setImageResource(R.drawable.regularincome);
        }

        return newView;
    }

}
