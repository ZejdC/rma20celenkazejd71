package com.example.spirala1.util;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.widget.Toast;

import com.example.spirala1.data.Payment;
import com.example.spirala1.data.Transaction;
import com.example.spirala1.list.TLI;

import java.text.SimpleDateFormat;


public class ConnectivityBroadcastReceiver extends BroadcastReceiver {
    private Cursor getCursor(String table, Context context){
        ContentResolver cr = context.getApplicationContext().getContentResolver();
        String where = null;
        String whereArgs[] = null;
        String order = null;
        String[] kolone = null;
        Uri adresa = null;
        switch (table){
            case "ADD":
                adresa = Uri.parse("content://spirala.provider.add/elements");
                kolone = new String[]{
                        TransactionDBOpenHelper.ID, TransactionDBOpenHelper.TITLE, TransactionDBOpenHelper.AMOUNT,
                        TransactionDBOpenHelper.TYPE, TransactionDBOpenHelper.DESCRIPTION, TransactionDBOpenHelper.INTERVAL,
                        TransactionDBOpenHelper.DATE, TransactionDBOpenHelper.ENDDATE};
                break;
            case "EDIT":
                adresa = Uri.parse("content://spirala.provider.edit/elements");
                kolone = new String[]{
                        TransactionDBOpenHelper.ID, TransactionDBOpenHelper.TITLE, TransactionDBOpenHelper.AMOUNT,
                        TransactionDBOpenHelper.TYPE, TransactionDBOpenHelper.DESCRIPTION, TransactionDBOpenHelper.INTERVAL,
                        TransactionDBOpenHelper.DATE, TransactionDBOpenHelper.ENDDATE};
                break;
            case "DELETE":
                adresa = Uri.parse("content://spirala.provider.delete/elements");
                kolone = new String[]{
                        TransactionDBOpenHelper.ID, TransactionDBOpenHelper.TITLE, TransactionDBOpenHelper.AMOUNT,
                        TransactionDBOpenHelper.TYPE, TransactionDBOpenHelper.DESCRIPTION, TransactionDBOpenHelper.INTERVAL,
                        TransactionDBOpenHelper.DATE, TransactionDBOpenHelper.ENDDATE
                };
                break;
            case "ACCOUNT":
                adresa = Uri.parse("content://spirala.provider.account/elements");
                kolone = new String[]{
                        TransactionDBOpenHelper.BUDGET, TransactionDBOpenHelper.MONTHLIMIT, TransactionDBOpenHelper.TOTALLIMIT
                };
                break;
            default:
                return null;
        };

        Cursor cur = cr.query(adresa,kolone,where,whereArgs,order);
        return cur;
    }
    private String getCursorValue(Cursor c, String s){
        return c.getString(c.getColumnIndexOrThrow(s));
    }

    private Payment getPayment(String type) {
        switch (type){
            case "Individual payment":
                return Payment.INDIVIDUALPAYMENT;
            case "Regular payment":
                return Payment.REGULARPAYMENT;
            case "Purchase":
                return Payment.PURCHASE;
            case "Individual income":
                return Payment.INDIVIDUALINCOME;
            case "Regular income":
                return Payment.REGULARINCOME;
            default:
                return null;
        }
    }
    @Override
    public void onReceive(Context context, Intent intent) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        assert cm != null;
        if (cm.getActiveNetworkInfo() == null) {
            Toast toast = Toast.makeText(context, "Disconnected", Toast.LENGTH_SHORT);
            toast.show();
        }
        else {
            Toast toast = Toast.makeText(context, "Connected", Toast.LENGTH_SHORT);
            toast.show();
            //SPREMI SVE NA SERVER
            String[] tabele = new String[]{"ADD","EDIT","DELETE","ACCOUNT"};
            for(String table: tabele){
                Cursor c = getCursor(table,context);
                while(c.moveToNext()){
                    if(table.equals("ADD")){
                        String title = getCursorValue(c,TransactionDBOpenHelper.TITLE);
                        Double amount = Double.valueOf(getCursorValue(c, TransactionDBOpenHelper.AMOUNT));
                        Payment payment = getPayment(getCursorValue(c,TransactionDBOpenHelper.TYPE));
                        String desciption = getCursorValue(c,TransactionDBOpenHelper.DESCRIPTION);
                        Integer interval = Integer.valueOf(getCursorValue(c, TransactionDBOpenHelper.INTERVAL));
                        String date = getCursorValue(c, TransactionDBOpenHelper.DATE);
                        String enddate = getCursorValue(c, TransactionDBOpenHelper.ENDDATE);
                        String[] pocetak = date.split("-");
                        String[] kraj = enddate.split("-");
                        String[] ddan = pocetak[2].split("T");
                        String[] edan = kraj[2].split("T");
                        Transaction t = new Transaction(Integer.valueOf(pocetak[0]),Integer.valueOf(pocetak[1]),Integer.valueOf(ddan[0]),
                                amount,title,payment,desciption,interval,Integer.valueOf(kraj[0]),Integer.valueOf(kraj[1]),Integer.valueOf(edan[0]));
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.000");

                        String[]argument = {"ADD",t.getTitle(),t.getAmount()+"",getId(t.getType().toString())+"",t.getItemDescription(),
                                t.getTransactionInterval()+"",sdf.format(t.getDate().getTime())+"Z",sdf.format(t.getEndDate().getTime())+"Z"};
                        new TLI().execute(argument);
                    }
                    else if(table.equals("EDIT")){
                        Integer id = Integer.valueOf(getCursorValue(c, TransactionDBOpenHelper.ID));
                        String title = getCursorValue(c,TransactionDBOpenHelper.TITLE);
                        Double amount = Double.valueOf(getCursorValue(c, TransactionDBOpenHelper.AMOUNT));
                        Payment payment = getPayment(getCursorValue(c,TransactionDBOpenHelper.TYPE));
                        String desciption = getCursorValue(c,TransactionDBOpenHelper.DESCRIPTION);
                        Integer interval = Integer.valueOf(getCursorValue(c, TransactionDBOpenHelper.INTERVAL));
                        String date = getCursorValue(c, TransactionDBOpenHelper.DATE);
                        String enddate = getCursorValue(c, TransactionDBOpenHelper.ENDDATE);
                        String[] pocetak = date.split("-");
                        String[] kraj = enddate.split("-");
                        String[] ddan = pocetak[2].split("T");
                        String[] edan = kraj[2].split("T");
                        Transaction t = new Transaction(Integer.valueOf(pocetak[0]),Integer.valueOf(pocetak[1]),Integer.valueOf(ddan[0]),
                                amount,title,payment,desciption,interval,Integer.valueOf(kraj[0]),Integer.valueOf(kraj[1]),Integer.valueOf(edan[0]),id);
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.000");
                        String[]argument = {"EDIT",t.getTitle(),t.getAmount()+"",getId(t.getType().toString())+"",t.getItemDescription(),
                                t.getTransactionInterval()+"",sdf.format(t.getDate().getTime())+"Z",sdf.format(t.getEndDate().getTime())+"Z",t.getId()+""};
                        new TLI().execute(argument);
                    }
                    else if(table.equals("DELETE")){
                        Integer id = Integer.valueOf(getCursorValue(c, TransactionDBOpenHelper.ID));
                        new TLI().execute(new String[]{"DELETE",id+""});
                    }
                    else{
                        Double budget = Double.valueOf(getCursorValue(c, TransactionDBOpenHelper.BUDGET));
                        Double month = Double.valueOf(getCursorValue(c, TransactionDBOpenHelper.MONTHLIMIT));
                        Double limit = Double.valueOf(getCursorValue(c, TransactionDBOpenHelper.TOTALLIMIT));
                        new TLI().execute(new String[]{"EDITACC",budget+"",limit+"",month+""});
                    }
                }
                //OBRISI
                if(table.equals("ADD")){
                    context.getContentResolver().delete(Uri.parse("content://spirala.provider.add"),null,null);
                }
                else if(table.equals("EDIT")){
                    context.getContentResolver().delete(Uri.parse("content://spirala.provider.edit"),null,null);
                }
                else if(table.equals("DELETE")){
                    context.getContentResolver().delete(Uri.parse("content://spirala.provider.delete"),null,null);
                }

                c.close();
            }

        }
    }

    private Integer getId(String type) {
        switch (type){
            case "Individual payment":
                return 5;
            case "Regular payment":
                return 1;
            case "Purchase":
                return 3;
            case "Individual income":
                return 4;
            case "Regular income":
                return 2;
            default:
                return null;
        }
    }
}
