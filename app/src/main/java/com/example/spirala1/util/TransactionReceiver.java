package com.example.spirala1.util;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class TransactionReceiver extends ResultReceiver {
    private Receiver receiver;
    public TransactionReceiver(Handler handler) {
        super(handler);
    }
    public void setReceiver(Receiver receiver){
        this.receiver = receiver;
    }
    public interface Receiver {
        public void onReceiveResult(int result, Bundle bundle);
    }
    @Override
    protected void onReceiveResult(int result, Bundle bundle){
        if(receiver!=null){
            receiver.onReceiveResult(result,bundle);
        }
    }
}
