package com.example.spirala1.util;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class TransactionDBOpenHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "SpiralaDB.db";
    public static final int DATABASE_VERSION = 3;

    public TransactionDBOpenHelper(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
        //context.deleteDatabase(DATABASE_NAME);
    }
    public TransactionDBOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    public static final String ADD_TABLE = "addtable";
    public static final String EDIT_TABLE = "edittable";
    public static final String DELETE_TABLE = "deletetable";
    public static final String ACCOUNT_TABLE = "account";
    public static final String BUDGET = "budget";
    public static final String MONTHLIMIT = "month";
    public static final String TOTALLIMIT = "total";
    public static final String ID = "id";
    public static final String TITLE = "title";
    public static final String AMOUNT = "amount";
    public static final String TYPE = "type";
    public static final String DESCRIPTION = "description";
    public static final String INTERVAL = "interval";
    public static final String DATE = "date";
    public static final String ENDDATE = "enddate";

    private static final String ACCOUNT_CREATE =
            "CREATE TABLE IF NOT EXISTS "+ ACCOUNT_TABLE
            + " ("+ BUDGET + " DECIMAL, "
            + MONTHLIMIT + " DECIMAL, "
            + TOTALLIMIT + " DECIMAL);";

    private static final String ADD_TABLE_CREATE =
            "CREATE TABLE IF NOT EXISTS " + ADD_TABLE
            + " ("+ ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + TITLE + " TEXT NOT NULL, "
            + AMOUNT + " DECIMAL, "
            + TYPE + " TEXT, "
            + DESCRIPTION + " TEXT, "
            + INTERVAL + " INTEGER, "
            + DATE + " TEXT, "
            + ENDDATE + " TEXT);";
    private static final String EDIT_TABLE_CREATE=
            "CREATE TABLE IF NOT EXISTS " + EDIT_TABLE
                    + " ("+ ID + " INTEGER PRIMARY KEY, "
                    + TITLE + " TEXT NOT NULL, "
                    + AMOUNT + " DECIMAL, "
                    + TYPE + " TEXT, "
                    + DESCRIPTION + " TEXT, "
                    + INTERVAL + " INTEGER, "
                    + DATE + " TEXT, "
                    + ENDDATE + " TEXT);";
    private static final String DELETE_TABLE_CREATE=
            "CREATE TABLE IF NOT EXISTS "+ DELETE_TABLE
                    + " ("+ ID + " INTEGER PRIMARY KEY, "
                    + TITLE + " TEXT NOT NULL, "
                    + AMOUNT + " DECIMAL, "
                    + TYPE + " TEXT, "
                    + DESCRIPTION + " TEXT, "
                    + INTERVAL + " INTEGER, "
                    + DATE + " TEXT, "
                    + ENDDATE + " TEXT);";

    private static final String ADD_TABLE_DROP = "DROP TABLE IF EXISTS "+ADD_TABLE;
    private static final String EDIT_TABLE_DROP = "DROP TABLE IF EXISTS "+EDIT_TABLE;
    private static final String DELETE_TABLE_DROP = "DROP TABLE IF EXISTS "+DELETE_TABLE;
    private static final String ACCOUNT_TABLE_DROP = "DROP TABLE IF EXISTS "+ACCOUNT_TABLE;

    private static final String ADD_ACCOUNT_INSTANCE = "INSERT INTO "+ACCOUNT_TABLE+" VALUES(5000,2000,3000)";

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(ADD_TABLE_CREATE);
        db.execSQL(EDIT_TABLE_CREATE);
        db.execSQL(DELETE_TABLE_CREATE);
        db.execSQL(ACCOUNT_CREATE);
        db.execSQL(ADD_ACCOUNT_INSTANCE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
//        db.execSQL(ADD_TABLE_DROP);
//        db.execSQL(EDIT_TABLE_DROP);
//        db.execSQL(DELETE_TABLE_DROP);
//        db.execSQL(ACCOUNT_TABLE_DROP);
    }
}
