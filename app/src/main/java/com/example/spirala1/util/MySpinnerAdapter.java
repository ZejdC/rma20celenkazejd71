package com.example.spirala1.util;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.spirala1.R;

import java.util.List;

public class MySpinnerAdapter extends ArrayAdapter<String> {
    int res;
    public MySpinnerAdapter(@NonNull Context context, int resource, int textViewResourceId, @NonNull List<String> objects) {
        super(context, resource, textViewResourceId, objects);
        res = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LinearLayout newView;
        if(convertView == null){
            newView = new LinearLayout(getContext());
            String inflater = Context.LAYOUT_INFLATER_SERVICE;
            LayoutInflater li;
            li = (LayoutInflater)getContext().getSystemService(inflater);
            li.inflate(res, newView, true);
        }
        else{
            newView = (LinearLayout)convertView;
        }
        String s = getItem(position);
        ImageView slika = (ImageView) newView.findViewById(R.id.slika);
        TextView title = (TextView) newView.findViewById(R.id.naziv);
        title.setText(s);
        if(s.equals("Regular payment")) {
            slika.setImageResource(R.drawable.regularpayment);
        }else if(s.equals("Individual payment")){
            slika.setImageResource(R.drawable.onetimepayment);
        }else if(s.equals("Purchase")){
            slika.setImageResource(R.drawable.purchase);
        }else if(s.equals("Individual income")){
            slika.setImageResource(R.drawable.individualincome);
        }else if(s.equals("Regular income")){
            slika.setImageResource(R.drawable.regularincome);
        }else{
            slika.setImageResource(android.R.drawable.screen_background_light_transparent);
        }

        return newView;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LinearLayout newView;
        if(convertView == null){
            newView = new LinearLayout(getContext());
            String inflater = Context.LAYOUT_INFLATER_SERVICE;
            LayoutInflater li;
            li = (LayoutInflater)getContext().getSystemService(inflater);
            li.inflate(res, newView, true);
        }
        else{
            newView = (LinearLayout)convertView;
        }
        String s = getItem(position);
        ImageView slika = (ImageView) newView.findViewById(R.id.slika);
        TextView title = (TextView) newView.findViewById(R.id.naziv);
        title.setText(s);
        if(s.equals("Regular payment")) {
            slika.setImageResource(R.drawable.regularpayment);
        }else if(s.equals("Individual payment")){
            slika.setImageResource(R.drawable.onetimepayment);
        }else if(s.equals("Purchase")){
            slika.setImageResource(R.drawable.purchase);
        }else if(s.equals("Individual income")){
            slika.setImageResource(R.drawable.individualincome);
        }else if(s.equals("Regular income")){
            slika.setImageResource(R.drawable.regularincome);
        }else{
            slika.setImageResource(android.R.drawable.screen_background_light_transparent);
        }

        return newView;
    }
}
