package com.example.spirala1.detail.graph;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.spirala1.R;
import com.example.spirala1.data.Transaction;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.LargeValueFormatter;

import java.util.ArrayList;
import java.util.List;

public class GraphsFragment extends Fragment implements IGraphView {
    View fragmentView;
    BarChart minus,plus,saldo;
    Spinner spinner;
    String[] mode= {"Daily","Weekly","Monthly"};

    private IGraphPresenter presenter;

    public  IGraphPresenter getPresenter(){
        if(presenter==null)presenter=new GraphPresenter(this,getActivity());
        return presenter;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentView = inflater.inflate(R.layout.fragment_graph,container,false);
        minus = fragmentView.findViewById(R.id.minus);
        plus = fragmentView.findViewById(R.id.plus);
        saldo = fragmentView.findViewById(R.id.saldo);
        spinner = fragmentView.findViewById(R.id.spinner);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item, mode);
        spinner.setAdapter(adapter);
        spinner.setSelection(2);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String s = (String) parent.getItemAtPosition(position);
                getPresenter().calculate(s);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        return fragmentView;
    }

    @Override
    public void show(List<Transaction> t) {
        if(spinner.getSelectedItem().toString().equals("Monthly")){
            ArrayList<BarEntry> barEntries = new ArrayList<>();
            ArrayList<Float> yaxis = getPresenter().giveMonthlyData(1);
            for(int i = 0; i < yaxis.size(); i++){
                barEntries.add(new BarEntry(i,yaxis.get(i)));
            }
            BarDataSet dataSet = new BarDataSet(barEntries, "Income");
            dataSet.setValueFormatter(new LargeValueFormatter());
            dataSet.setColor(Color.GREEN);

            BarData barData = new BarData(dataSet);
            Description desc = new Description();
            desc.setText("");
            plus.setDescription(desc);
            plus.setData(barData);

            barEntries = new ArrayList<>();
            yaxis = getPresenter().giveMonthlyData(-1);
            for(int i = 0; i < yaxis.size(); i++){
                barEntries.add(new BarEntry(i,yaxis.get(i)));
            }
            dataSet = new BarDataSet(barEntries, "Expense");
            dataSet.setValueFormatter(new LargeValueFormatter());
            dataSet.setColor(Color.RED);

            barData = new BarData(dataSet);
            minus.setDescription(desc);
            minus.setData(barData);

            barEntries = new ArrayList<>();
            yaxis = getPresenter().giveMonthlyBalance();
            for(int i = 0; i < yaxis.size(); i++){
                barEntries.add(new BarEntry(i,yaxis.get(i)));
            }
            dataSet = new BarDataSet(barEntries, "Balance");
            dataSet.setValueFormatter(new LargeValueFormatter());
            dataSet.setColor(Color.YELLOW);

            barData = new BarData(dataSet);
            saldo.setDescription(desc);
            saldo.setData(barData);
        }else if(spinner.getSelectedItem().toString().equals("Daily")){
            ArrayList<BarEntry> barEntries = new ArrayList<>();
            ArrayList<Float> yaxis = getPresenter().giveDailyData(1);
            for(int i = 0; i < yaxis.size(); i++){
                barEntries.add(new BarEntry(i,yaxis.get(i)));
            }
            BarDataSet dataSet = new BarDataSet(barEntries, "Income");
            dataSet.setValueFormatter(new LargeValueFormatter());
            dataSet.setColor(Color.GREEN);

            BarData barData = new BarData(dataSet);
            Description desc = new Description();
            desc.setText("");
            plus.setDescription(desc);
            plus.setData(barData);

            barEntries = new ArrayList<>();
            yaxis = getPresenter().giveDailyData(-1);
            for(int i = 0; i < yaxis.size(); i++){
                barEntries.add(new BarEntry(i,yaxis.get(i)));
            }
            dataSet = new BarDataSet(barEntries, "Expense");
            dataSet.setValueFormatter(new LargeValueFormatter());
            dataSet.setColor(Color.RED);

            barData = new BarData(dataSet);
            minus.setDescription(desc);
            minus.setData(barData);

            barEntries = new ArrayList<>();
            yaxis = getPresenter().giveDailyBalance();
            for(int i = 0; i < yaxis.size(); i++){
                barEntries.add(new BarEntry(i,yaxis.get(i)));
            }
            dataSet = new BarDataSet(barEntries, "Balance");
            dataSet.setValueFormatter(new LargeValueFormatter());
            dataSet.setColor(Color.YELLOW);

            barData = new BarData(dataSet);
            saldo.setDescription(desc);
            saldo.setData(barData);
        }else{
            ArrayList<BarEntry> barEntries = new ArrayList<>();
            ArrayList<Float> yaxis = getPresenter().giveWeeklyData(1);
            for(int i = 0; i < yaxis.size(); i++){
                barEntries.add(new BarEntry(i,yaxis.get(i)));
            }
            BarDataSet dataSet = new BarDataSet(barEntries, "Income");
            dataSet.setValueFormatter(new LargeValueFormatter());
            dataSet.setColor(Color.GREEN);

            BarData barData = new BarData(dataSet);
            Description desc = new Description();
            desc.setText("");
            plus.setDescription(desc);
            plus.setData(barData);

            barEntries = new ArrayList<>();
            yaxis = getPresenter().giveWeeklyData(-1);
            for(int i = 0; i < yaxis.size(); i++){
                barEntries.add(new BarEntry(i,yaxis.get(i)));
            }
            dataSet = new BarDataSet(barEntries, "Expense");
            dataSet.setValueFormatter(new LargeValueFormatter());
            dataSet.setColor(Color.RED);

            barData = new BarData(dataSet);
            minus.setDescription(desc);
            minus.setData(barData);

            barEntries = new ArrayList<>();
            yaxis = getPresenter().giveWeeklyBalance();
            for(int i = 0; i < yaxis.size(); i++){
                barEntries.add(new BarEntry(i,yaxis.get(i)));
            }
            dataSet = new BarDataSet(barEntries, "Balance");
            dataSet.setValueFormatter(new LargeValueFormatter());
            dataSet.setColor(Color.YELLOW);

            barData = new BarData(dataSet);
            saldo.setDescription(desc);
            saldo.setData(barData);
        }
        plus.invalidate();
        minus.invalidate();
        saldo.invalidate();
    }
}
