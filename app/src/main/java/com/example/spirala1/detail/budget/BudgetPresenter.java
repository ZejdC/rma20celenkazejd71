package com.example.spirala1.detail.budget;

import android.content.ContentValues;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.widget.Toast;

import androidx.fragment.app.FragmentActivity;

import com.example.spirala1.data.Account;
import com.example.spirala1.data.Transaction;
import com.example.spirala1.detail.graph.IGraphInteractor;
import com.example.spirala1.list.TLI;
import com.example.spirala1.util.TransactionDBOpenHelper;

import java.util.ArrayList;

public class BudgetPresenter implements IBudgetPresenter, TLI.interactorInterface {
    private IBudgetInteractor interactor;
    Context context;
    IBudgetView view;

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public BudgetPresenter(IBudgetView budgetFragment, Context activity) {
        view = budgetFragment;
        context = activity;
        new TLI(this).execute(new String[]{"GETACC"});
    }

    @Override
    public void save(double b, double tl, double ml) {
        Account account = new Account(b,tl,ml);
        if(isNetworkAvailable())
        new TLI(this).execute(new String[]{"EDITACC",b+"",tl+"",ml+""});
        ContentValues cv = new ContentValues();
        cv.put(TransactionDBOpenHelper.BUDGET,b);
        cv.put(TransactionDBOpenHelper.MONTHLIMIT,ml);
        cv.put(TransactionDBOpenHelper.TOTALLIMIT,tl);
        context.getContentResolver().update(Uri.parse("content://spirala.provider.account/elements"),cv,null,null);
        Toast.makeText(context, "Saved account", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDone(ArrayList<Transaction> results, Account account) {
        if(account!=null)
        view.updateAccount(account);
    }

    @Override
    public void saveCurrentState(ArrayList<Transaction> results, Account account) {

    }
}
