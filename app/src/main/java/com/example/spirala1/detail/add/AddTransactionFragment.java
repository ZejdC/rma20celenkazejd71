package com.example.spirala1.detail.add;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.spirala1.R;
import com.example.spirala1.data.Account;
import com.example.spirala1.data.Payment;
import com.example.spirala1.data.Transaction;
import com.example.spirala1.util.MySpinnerAdapter;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;

public class AddTransactionFragment extends Fragment {
    View fragmentView;
    EditText title;
    EditText amount;
    EditText date;
    Spinner type;
    EditText enddate;
    EditText interval;
    EditText description;
    Button l,m,r;
    String[] types = {"Regular payment","Individual payment","Purchase","Regular income","Individual income"};
    TextView balance;
    TextView limit;
    TextView status;

    private Payment getPayment(String type) {
        switch (type){
            case "Individual payment":
                return Payment.INDIVIDUALPAYMENT;
            case "Regular payment":
                return Payment.REGULARPAYMENT;
            case "Purchase":
                return Payment.PURCHASE;
            case "Individual income":
                return Payment.INDIVIDUALINCOME;
            case "Regular income":
                return Payment.REGULARINCOME;
            default:
                return null;
        }
    }

    public interface OnItemClick {
        public void onSaveClicked(Bundle bundle);
    }

    private OnItemClick onItemClick;

    private boolean isRegularType() {
        return type.getSelectedItem().toString().equals("Regular income")||type.getSelectedItem().toString().equals("Regular payment");
    }

    private boolean isIncomeType() {
        return type.getSelectedItem().toString().equals("Regular income")||type.getSelectedItem().toString().equals("Individual income");
    }
    private boolean check(){
        if(isRegularType()){
            if(isIncomeType()){
                return title.getText().toString().length()>3&&title.getText().toString().length()<15&&!amount.getText().toString().isEmpty()
                        &&!interval.getText().toString().isEmpty();
            }
            else{
                return title.getText().toString().length()>3&&title.getText().toString().length()<15&&!amount.getText().toString().isEmpty()
                        &&!interval.getText().toString().isEmpty()&&!description.getText().toString().isEmpty();
            }
        }
        else{
            if(isIncomeType()){
                return title.getText().toString().length()>3&&title.getText().toString().length()<15&&!amount.getText().toString().isEmpty();
            }
            else{
                return title.getText().toString().length()>3&&title.getText().toString().length()<15&&!amount.getText().toString().isEmpty()
                        &&!description.getText().toString().isEmpty();
            }
        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
    
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentView = inflater.inflate(R.layout.fragment_detail,container,false);
        onItemClick = (OnItemClick) getActivity();

        status = fragmentView.findViewById(R.id.status);

        if(!isNetworkAvailable()){
            status.setText("Offline dodavanje");
        }

        final SimpleDateFormat[] sdf = {new SimpleDateFormat("dd-MMMM-YYYY")};
        Calendar prvi = Calendar.getInstance();
        Calendar drugi = Calendar.getInstance();

        balance = fragmentView.findViewById(R.id.balance);
        limit = fragmentView.findViewById(R.id.limit);

        title = fragmentView.findViewById(R.id.editTitle);
        amount = fragmentView.findViewById(R.id.editAmount);
        date = fragmentView.findViewById(R.id.editDate);
        type = fragmentView.findViewById(R.id.editType);
        enddate = fragmentView.findViewById(R.id.editEnd);
        interval = fragmentView.findViewById(R.id.editInterval);
        description = fragmentView.findViewById(R.id.editDescription);
        l = fragmentView.findViewById(R.id.buttonSave);
        m = fragmentView.findViewById(R.id.buttonDelete);
        r = fragmentView.findViewById(R.id.buttonClose);
        type.setAdapter(new MySpinnerAdapter(getContext(),R.layout.spinnerelement,R.id.naziv, Arrays.asList(types)));
        type.setSelection(0);
        if(getArguments()!=null) {
            Account account = getArguments().getParcelable("account");
            if(account!=null) {
                balance.setText(Double.toString(account.getBalance()));
                limit.setText(Double.toString(account.getTotalLimit()));
            }
        }

        date.setText(sdf[0].format(prvi.getTime()));
        enddate.setText(sdf[0].format(drugi.getTime()));

        DatePickerDialog.OnDateSetListener datum1 = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                prvi.set(Calendar.YEAR, year);
                prvi.set(Calendar.MONTH, monthOfYear);
                prvi.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MMMM-YYYY");
                date.setText(sdf.format(prvi.getTime()));
            }
        };
        DatePickerDialog.OnDateSetListener datum2 = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                drugi.set(Calendar.YEAR, year);
                drugi.set(Calendar.MONTH, monthOfYear);
                drugi.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MMMM-YYYY");
                enddate.setText(sdf.format(drugi.getTime()));
            }
        };
        date.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(getContext(), datum1, prvi
                        .get(Calendar.YEAR), prvi.get(Calendar.MONTH),
                        prvi.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        enddate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(getContext(), datum2, drugi
                        .get(Calendar.YEAR), drugi.get(Calendar.MONTH),
                        drugi.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        m.setEnabled(false);
        l.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if ((Double.parseDouble(amount.getText().toString()) > Double.parseDouble(balance.getText().toString())) || Double.parseDouble(amount.getText().toString()) > Double.parseDouble(limit.getText().toString())) {
                        new AlertDialog.Builder(getContext()).setTitle("Edit entry")
                                .setMessage("This transaction would exceed your balance/limit. Are you sure you want to continue?")
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        if (check()) {
                                            int i;
                                            try{
                                                i = Integer.parseInt(interval.getText().toString());
                                            }catch (Exception e){
                                                i = 0;
                                            }
                                            Bundle bundle = new Bundle();
                                            bundle.putString("add","add");
                                            bundle.putParcelable("transaction",new Transaction(prvi.getTime(),drugi.getTime(),title.getText().toString(),Double.parseDouble(amount.getText().toString()),
                                                    getPayment((String) type.getSelectedItem()),description.getText().toString(),i));
                                            onItemClick.onSaveClicked(bundle);
                                        } else {
                                            new AlertDialog.Builder(getContext()).setTitle("Error")
                                                    .setMessage("Incorrect information!").show();
                                        }
                                    }
                                })
                                .setNegativeButton(android.R.string.no, null)
                                .setIcon(android.R.drawable.ic_dialog_alert).show();
                    }
                    else{
                        if (check()) {
                            int i;
                            try{
                                i = Integer.parseInt(interval.getText().toString());
                            }catch (Exception e){
                                i = 0;
                            }
                            Bundle bundle = new Bundle();
                            bundle.putString("add","add");
                            bundle.putParcelable("transaction",new Transaction(prvi.getTime(),drugi.getTime(),title.getText().toString(),Double.parseDouble(amount.getText().toString()),
                                    getPayment((String) type.getSelectedItem()),description.getText().toString(),i));
                            onItemClick.onSaveClicked(bundle);
                        } else {
                            new AlertDialog.Builder(getContext()).setTitle("Error")
                                    .setMessage("Incorrect information!").show();
                        }
                    }
                }catch(Exception e){
                    new AlertDialog.Builder(getContext()).setTitle("Error")
                            .setMessage("Incorrect information!").show();
                }
            }
        });
        r.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                finish();
            }
        });
        type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(isRegularType()){
                    enddate.setEnabled(true);
                    enddate.setText(date.getText().toString());
                    interval.setEnabled(true);
                    if(!isIncomeType()){
                        description.setEnabled(true);
                    }
                    else{
                        description.setEnabled(false);
                    }
                }
                else{
                    enddate.setEnabled(false);
                    interval.setEnabled(false);
                    if(!isIncomeType()){
                        description.setEnabled(true);
                    }
                    else{
                        description.setEnabled(false);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        return fragmentView;
    }
}
