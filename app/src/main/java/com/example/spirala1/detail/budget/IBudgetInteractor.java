package com.example.spirala1.detail.budget;

import com.example.spirala1.data.Account;
import com.example.spirala1.data.Transaction;

import java.util.ArrayList;

public interface IBudgetInteractor {
    ArrayList<Transaction> getTransactions();
    Account getAccount();
    void setAccount(Account account);
}
