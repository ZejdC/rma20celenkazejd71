package com.example.spirala1.detail.detail;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.spirala1.R;
import com.example.spirala1.data.Account;
import com.example.spirala1.data.Payment;
import com.example.spirala1.data.Transaction;
import com.example.spirala1.util.MySpinnerAdapter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;

public class TransactionDetailFragment extends Fragment {
    EditText title;
    EditText amount;
    EditText date;
    Spinner type;
    EditText enddate;
    EditText interval;
    EditText description;
    Button l,m,r;
    String currType;
    TextView balance;
    TextView limit;
    TextView status;
    double change=0;
    boolean obrisana = false;

    String[] types = {"Regular payment","Individual payment","Purchase","Regular income","Individual income"};

    public interface OnItemClicked{
        public void onDeleteClicked();
        public void onDetailsClosedClicked(Bundle bundle);

        void onUndoClicked();
    }

    private OnItemClicked onItemClicked;

    private Payment getPayment(String type) {
        switch (type){
            case "Individual payment":
                return Payment.INDIVIDUALPAYMENT;
            case "Regular payment":
                return Payment.REGULARPAYMENT;
            case "Purchase":
                return Payment.PURCHASE;
            case "Individual income":
                return Payment.INDIVIDUALINCOME;
            case "Regular income":
                return Payment.REGULARINCOME;
            default:
                return null;
        }
    }

    //LAST CURRENT SAVED

    String[] dtitle,damount,dtype,dinterval,ddescription;
    Calendar ddate = Calendar.getInstance();
    Calendar dend = Calendar.getInstance();

    Calendar prvi = Calendar.getInstance();
    Calendar drugi = Calendar.getInstance();

    int id;

    private boolean isIncomeType(){
        return type.getSelectedItem().toString().equals("Regular income")||type.getSelectedItem().toString().equals("Individual income");
    }
    private boolean isRegularType(){
        return type.getSelectedItem().toString().equals("Regular income")||type.getSelectedItem().toString().equals("Regular payment");
    }
    private int addOrSubtract(){
        if(isIncomeType())return 1;
        return -1;
    }

    private void saveCurrentState(){
        //PROVJERA JESU LI PODACI OK
        if(!drugi.before(prvi) && title.getText().toString().length()>3&&title.getText().toString().length()<15) {
            double change=0;
            if(isIncomeType()&&(dtype[0].equals("Regular income")||dtype[0].equals("Individual income"))){
                change = Double.parseDouble(amount.getText().toString()) - Double.parseDouble(damount[0]);
            }
            else if(!isIncomeType() && (dtype[0].equals("Regular payment")||dtype[0].equals("Indiviual payment")||dtype[0].equals("Purchase"))){
                change = -Double.parseDouble(amount.getText().toString()) + Double.parseDouble(damount[0]);
            }
            else if(isIncomeType() && (dtype[0].equals("Regular payment")||dtype[0].equals("Indiviual payment")||dtype[0].equals("Purchase"))){
                change = Double.parseDouble(amount.getText().toString()) + Double.parseDouble(damount[0]);
            }
            else if(!isIncomeType() && (dtype[0].equals("Regular income")||dtype[0].equals("Individual income"))){
                change = -Double.parseDouble(amount.getText().toString()) - Double.parseDouble(damount[0]);
            }
            dtitle[0] = title.getText().toString();
            damount[0] = amount.getText().toString();
            dinterval[0] = interval.getText().toString();
            ddescription[0] = description.getText().toString() + "";
            dtype[0] = (String) type.getSelectedItem();
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MMMM-YYYY");
            try {
                ddate.setTime((sdf.parse(date.getText().toString())));
                dend.setTime((sdf.parse(enddate.getText().toString())));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            title.setBackgroundColor(Color.TRANSPARENT);
            amount.setBackgroundColor(Color.TRANSPARENT);
            interval.setBackgroundColor(Color.TRANSPARENT);
            description.setBackgroundColor(Color.TRANSPARENT);
            type.setBackgroundColor(Color.TRANSPARENT);
            date.setBackgroundColor(Color.TRANSPARENT);
            enddate.setBackgroundColor(Color.TRANSPARENT);
            double priv = Double.parseDouble(balance.getText().toString()) + change;
            balance.setText(Double.toString(priv));
        }
        else{
            new AlertDialog.Builder(getContext()).setTitle("Error")
                    .setMessage("Incorrect information!").show();
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_detail, container, false);
        if(getArguments()!=null && getArguments().containsKey("account")&&getArguments().containsKey("transaction")){
            Transaction t = getArguments().getParcelable("transaction");
            id = t.getId();
            dtitle = new String[]{t.getTitle()};
            damount = new String[]{String.valueOf(t.getAmount())};
            dtype = new String[]{t.getType().toString()};
            dinterval = new String[]{String.valueOf(t.getTransactionInterval())};
            ddescription = new String[]{t.getItemDescription()};

            status = fragmentView.findViewById(R.id.status);
            obrisana = t.isObrisana();

            onItemClicked = (OnItemClicked) getActivity();

            ddate.setTime(t.getDate().getTime());
            dend.setTime(t.getEndDate().getTime());

            prvi.setTime(t.getDate().getTime());
            drugi.setTime(t.getEndDate().getTime());
            title = fragmentView.findViewById(R.id.editTitle);
            amount = fragmentView.findViewById(R.id.editAmount);
            date = fragmentView.findViewById(R.id.editDate);
            type = fragmentView.findViewById(R.id.editType);
            enddate = fragmentView.findViewById(R.id.editEnd);
            interval = fragmentView.findViewById(R.id.editInterval);
            description = fragmentView.findViewById(R.id.editDescription);
            l = fragmentView.findViewById(R.id.buttonSave);
            m = fragmentView.findViewById(R.id.buttonDelete);
            r = fragmentView.findViewById(R.id.buttonClose);
            balance = fragmentView.findViewById(R.id.balance);
            limit = fragmentView.findViewById(R.id.limit);

            if(!isNetworkAvailable()){
                if(!t.isObrisana())
                    status.setText("Offline izmjena");
                else {
                    status.setText("Offline brisanje");
                    title.setEnabled(false);
                    amount.setEnabled(false);
                    date.setEnabled(false);
                    type.setEnabled(false);
                    enddate.setEnabled(false);
                    interval.setEnabled(false);
                    description.setEnabled(false);
                    l.setEnabled(false);
                    r.setEnabled(false);
                    m.setText("UNDO");
                }

            }

            Account account = getArguments().getParcelable("account");
            if(account!=null) {
                balance.setText(Double.toString(account.getBalance()));
                limit.setText(Double.toString(account.getTotalLimit()));
            }

            final SimpleDateFormat[] sdf = {new SimpleDateFormat("dd-MMMM-YYYY")};

            type.setAdapter(new MySpinnerAdapter(getContext(),R.layout.spinnerelement,R.id.naziv, Arrays.asList(types)));
            int i = 0;
            for(;i<5;i++){
                if(types[i].equals(dtype[0]))break;
            }
            type.setSelection(i);
            currType = type.getSelectedItem().toString();
            if(isIncomeType()){
                description.setText("");
                description.setEnabled(false);
            }
            else{
                description.setText(t.getItemDescription());
            }
            if(isRegularType()){
                interval.setText(String.valueOf(t.getTransactionInterval()));
                enddate.setText(sdf[0].format(drugi.getTime()));

            }
            else{
                interval.setText("");
                enddate.setText("");
                enddate.setEnabled(false);
                interval.setEnabled(false);
            }


            title.setText(t.getTitle());

            amount.setText(String.valueOf(t.getAmount()));
            date.setText(sdf[0].format(prvi.getTime()));


            DatePickerDialog.OnDateSetListener datum1 = new DatePickerDialog.OnDateSetListener() {

                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear,
                                      int dayOfMonth) {
                    // TODO Auto-generated method stub
                    prvi.set(Calendar.YEAR, year);
                    prvi.set(Calendar.MONTH, monthOfYear);
                    prvi.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MMMM-YYYY");
                    date.setText(sdf.format(prvi.getTime()));
                    if(!date.equals(ddate))
                        date.setBackgroundColor(Color.parseColor("#22bb45"));
                    else
                        date.setBackgroundColor(Color.TRANSPARENT);
                }

            };
            DatePickerDialog.OnDateSetListener datum2 = new DatePickerDialog.OnDateSetListener() {

                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear,
                                      int dayOfMonth) {
                    // TODO Auto-generated method stub
                    drugi.set(Calendar.YEAR, year);
                    drugi.set(Calendar.MONTH, monthOfYear);
                    drugi.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MMMM-YYYY");
                    enddate.setText(sdf.format(drugi.getTime()));
                    if(!enddate.equals(dend))
                        enddate.setBackgroundColor(Color.parseColor("#22bb45"));
                    else
                        enddate.setBackgroundColor(Color.TRANSPARENT);
                }
            };

            date.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    new DatePickerDialog(getContext(), datum1, prvi
                            .get(Calendar.YEAR), prvi.get(Calendar.MONTH),
                            prvi.get(Calendar.DAY_OF_MONTH)).show();
                }
            });
            enddate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    new DatePickerDialog(getContext(), datum2, drugi
                            .get(Calendar.YEAR), drugi.get(Calendar.MONTH),
                            drugi.get(Calendar.DAY_OF_MONTH)).show();
                }
            });
            title.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if(s.length()<4||s.length()>14){
                        title.setBackgroundColor(Color.parseColor("#d86c70"));
                    }
                    else if(!dtitle[0].equals(s.toString()))
                        title.setBackgroundColor(Color.parseColor("#22bb45"));
                    else
                        title.setBackgroundColor(Color.TRANSPARENT);
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
            amount.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if(!damount[0].equals(s.toString()))
                        amount.setBackgroundColor(Color.parseColor("#22bb45"));
                    else
                        amount.setBackgroundColor(Color.TRANSPARENT);
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
            interval.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if(!dinterval[0].equals(s.toString()))
                        interval.setBackgroundColor(Color.parseColor("#22bb45"));
                    else
                        interval.setBackgroundColor(Color.TRANSPARENT);
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
            description.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if(!ddescription[0].equals(s.toString()))
                        description.setBackgroundColor(Color.parseColor("#22bb45"));
                    else
                        description.setBackgroundColor(Color.TRANSPARENT);
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });

            int finalI = i;
            type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if(position != finalI){
                        type.setBackgroundColor(Color.parseColor("#22bb45"));
                    }
                    else{
                        type.setBackgroundColor(Color.TRANSPARENT);
                    }
                    if(isRegularType()){
                        enddate.setEnabled(true);
                        drugi=t.getEndDate();
                        enddate.setText(sdf[0].format(drugi.getTime()));
                        interval.setEnabled(true);
                        if(!isIncomeType()){
                            description.setEnabled(true);
                        }
                        else{
                            description.setEnabled(false);
                        }
                    }
                    else{
                        enddate.setEnabled(false);
                        interval.setEnabled(false);
                        if(!isIncomeType()){
                            description.setEnabled(true);
                        }
                        else{
                            description.setEnabled(false);
                        }
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
            l.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        if ((Double.parseDouble(amount.getText().toString()) > Double.parseDouble(balance.getText().toString()))
                                || Double.parseDouble(amount.getText().toString()) > Double.parseDouble(limit.getText().toString())) {
                            new AlertDialog.Builder(getContext()).setTitle("Edit entry")
                                    .setMessage("This transaction would exceed your balance/limit. Are you sure you want to continue?")
                                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            saveCurrentState();
                                        }
                                    })
                                    .setNegativeButton(android.R.string.no, null)
                                    .setIcon(android.R.drawable.ic_dialog_alert).show();
                        } else {
                            saveCurrentState();
                        }
                    }catch(Exception e){
                        new AlertDialog.Builder(getContext()).setTitle("Error")
                                .setMessage("Incorrect information!").show();
                    }
                }
            });
            m.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(isNetworkAvailable()||!obrisana) {
                        new AlertDialog.Builder(getContext())
                                .setTitle("Delete entry")
                                .setMessage("Are you sure you want to delete this entry?")
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        onItemClicked.onDeleteClicked();
                                    }
                                })
                                .setNegativeButton(android.R.string.no, null)
                                .setIcon(android.R.drawable.ic_dialog_alert).show();
                    }else{
                        onItemClicked.onUndoClicked();
                    }

                }
            });
            r.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int i;
                    try{
                        i = Integer.parseInt(dinterval[0]);
                    }catch (Exception e){
                        i = 0;
                    }
                    Bundle bundle = new Bundle();
                    bundle.putString("edit","edit");
                    Transaction t = new Transaction(prvi.getTime(),drugi.getTime(),dtitle[0],Double.parseDouble(damount[0]),getPayment((String) type.getSelectedItem()),
                            ddescription[0],i,id);
                    bundle.putParcelable("transaction",t);
                    bundle.putString("balance",balance.getText().toString());
                    bundle.putString("limit",limit.getText().toString());
                    onItemClicked.onDetailsClosedClicked(bundle);
                }
            });
        }
        return fragmentView;
    }

}
