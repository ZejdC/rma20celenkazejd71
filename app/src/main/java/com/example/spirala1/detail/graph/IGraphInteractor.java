package com.example.spirala1.detail.graph;

import com.example.spirala1.data.Account;
import com.example.spirala1.data.Transaction;

import java.util.ArrayList;

public interface IGraphInteractor {
    ArrayList<Transaction> getTransactions();
    Account getAccount();
}
