package com.example.spirala1.detail.budget;

import android.view.Display;

import com.example.spirala1.data.Account;
import com.example.spirala1.data.Model;
import com.example.spirala1.data.Transaction;

import java.util.ArrayList;

public class BudgetInteractor implements IBudgetInteractor {
    @Override
    public ArrayList<Transaction> getTransactions() {
        return (ArrayList<Transaction>) Model.transactions;
    }

    @Override
    public Account getAccount() {
        return Model.account;
    }

    @Override
    public void setAccount(Account account) {
        Model.account.setBalance(account.getBalance());
        Model.account.setMonthLimit(account.getMonthLimit());
        Model.account.setTotalLimit(account.getTotalLimit());
    }
}
