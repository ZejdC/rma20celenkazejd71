package com.example.spirala1.detail.graph;

import com.example.spirala1.data.Account;
import com.example.spirala1.data.Model;
import com.example.spirala1.data.Transaction;

import java.util.ArrayList;

public class GraphInteractor implements IGraphInteractor {
    @Override
    public ArrayList<Transaction> getTransactions() {
        return (ArrayList<Transaction>) Model.transactions;
    }

    @Override
    public Account getAccount() {
        return Model.account;
    }
}
