package com.example.spirala1.detail.graph;

import com.example.spirala1.data.Transaction;

import java.util.List;

public interface IGraphView {
    void show(List<Transaction> t);
}
