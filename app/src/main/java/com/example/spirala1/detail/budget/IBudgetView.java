package com.example.spirala1.detail.budget;

import com.example.spirala1.data.Account;

public interface IBudgetView {
    void updateAccount(Account account);
}
