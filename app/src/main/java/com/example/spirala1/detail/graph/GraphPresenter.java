package com.example.spirala1.detail.graph;

import android.content.Context;

import com.example.spirala1.data.Account;
import com.example.spirala1.data.Payment;
import com.example.spirala1.data.Transaction;
import com.example.spirala1.list.TLI;

import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class GraphPresenter implements IGraphPresenter, TLI.interactorInterface {
    Context context;
    IGraphView view;
    List<Transaction> trans;
    Calendar calendarMin = Calendar.getInstance();
    Calendar calendarMax = Calendar.getInstance();

    private boolean isIncomeType(Transaction t){
        return t.getType().equals(Payment.REGULARINCOME)||t.getType().equals(Payment.INDIVIDUALINCOME);
    }
    private boolean isRegularType(Transaction t){
        return t.getType().equals(Payment.REGULARINCOME)||t.getType().equals(Payment.REGULARPAYMENT);
    }

    public GraphPresenter(IGraphView view, Context context) {
        this.context = context;
        this.view = view;
    }

    private boolean check(Transaction t, Calendar c){
        if(t.getDate().equals(c))return true;
        else if(!isRegularType(t))return false;

        Calendar temp = (Calendar) t.getDate().clone();

        while(temp.before(c)){
            temp.add(Calendar.DAY_OF_MONTH,t.getTransactionInterval());
        }
        return temp.equals(c);
    }

    private ArrayList<Transaction> giveTransactionOnCurrentDay(Calendar calendar){
        ArrayList<Transaction> transactions = new ArrayList<>();
        for(Transaction t: trans){
            if(check(t,calendar)){
                transactions.add(t);
            }
        }
        return transactions;
    }

    private ArrayList<Transaction> giveTransactionsOnCurrentMonth(Calendar calendar){
        Calendar temp = (Calendar) calendar.clone();
        int days = temp.getActualMaximum(Calendar.DAY_OF_MONTH);
        temp.set(Calendar.DAY_OF_MONTH,1);
        ArrayList<Transaction> transactions = new ArrayList<>();
        for(int i = 0; i < days; i++){
            for(Transaction t: trans){
                if(check(t,temp)){
                    transactions.add(t);
                }
            }
            temp.add(Calendar.DAY_OF_MONTH,1);
        }
        return transactions;
    }
    private ArrayList<Transaction> giveTransactionsOnCurrentWeek(Calendar calendar){
        Calendar temp = (Calendar) calendar.clone();
        temp.set(Calendar.DAY_OF_WEEK,1);
        ArrayList<Transaction> transactions = new ArrayList<>();
        for(int i = 0; i < 7; i++){
            for(Transaction t: trans){
                if(check(t,temp)){
                    transactions.add(t);
                }
            }
            temp.add(Calendar.DAY_OF_MONTH,1);
        }
        return transactions;
    }

    @Override
    public ArrayList<Float> giveDailyData(int mode) {
        Calendar calendarMin = Calendar.getInstance();

        calendarMin.set(Calendar.DAY_OF_MONTH,1);
        calendarMin.set(Calendar.HOUR_OF_DAY, 0);
        calendarMin.set(Calendar.MINUTE, 0);
        calendarMin.set(Calendar.SECOND, 0);
        calendarMin.set(Calendar.MILLISECOND, 0);

        int days = Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH);
        ArrayList<Float>vrati=new ArrayList<>();
        Calendar pom = (Calendar) calendarMin.clone();
        for(int i = 0; i < days; i++){
            float value = 0;
            for(Transaction t: giveTransactionOnCurrentDay(pom)){
                if(mode>=0&&t.isIncomeType()){
                    value+=t.getAmount();
                }
                else if(mode<0&&!t.isIncomeType()){
                    value+=t.getAmount();
                }
            }
            pom.add(Calendar.DAY_OF_MONTH,1);
            vrati.add(value);
        }
        return vrati;
    }

    @Override
    public ArrayList<Float> giveMonthlyData(int mode) {
        Calendar calendarMin = Calendar.getInstance();

        calendarMin.set(Calendar.MONTH,0);
        calendarMin.set(Calendar.HOUR_OF_DAY, 0);
        calendarMin.set(Calendar.MINUTE, 0);
        calendarMin.set(Calendar.SECOND, 0);
        calendarMin.set(Calendar.MILLISECOND, 0);

        int months = 12;
        ArrayList<Float>vrati=new ArrayList<>();
        Calendar pom = (Calendar) calendarMin.clone();
        for(int i = 0; i < months; i++){
            float value = 0;
            for(Transaction t: giveTransactionsOnCurrentMonth(pom)){
                if(mode>=0&&t.isIncomeType()){
                    value+=t.getAmount();
                }
                else if(mode<0&&!t.isIncomeType()){
                    value+=t.getAmount();
                }
            }
            pom.add(Calendar.MONTH,1);
            vrati.add(value);
        }
        return vrati;
    }

    @Override
    public ArrayList<Float> giveWeeklyData(int mode) {
        Calendar calendarMin = Calendar.getInstance();

        calendarMin.set(Calendar.HOUR_OF_DAY, 0);
        calendarMin.set(Calendar.MINUTE, 0);
        calendarMin.set(Calendar.SECOND, 0);
        calendarMin.set(Calendar.MILLISECOND, 0);

        //CETIRI SEDMICE NAZAD

        calendarMin.add(Calendar.DAY_OF_MONTH,-28);

        ArrayList<Float>vrati=new ArrayList<>();
        Calendar pom = (Calendar) calendarMin.clone();
        for(int i = 0; i < 9; i++){
            float value = 0;
            for(Transaction t: giveTransactionsOnCurrentWeek(pom)){
                if(mode>=0&&t.isIncomeType()){
                    value+=t.getAmount();
                }
                else if(mode<0&&!t.isIncomeType()){
                    value+=t.getAmount();
                }
            }
            pom.add(Calendar.DAY_OF_MONTH,7);
            vrati.add(value);
        }
        return vrati;
    }

    @Override
    public ArrayList<Float> giveDailyBalance() {
        ArrayList<Float> income = giveDailyData(1);
        ArrayList<Float> expense = giveDailyData(-1);
        ArrayList<Float> endResult = new ArrayList<>();
        for(int i = 0; i < income.size(); i++){
            if(i==0)
                endResult.add(income.get(i)-expense.get(i));
            else
                endResult.add(endResult.get(i-1)+income.get(i)-expense.get(i));
        }
        return  endResult;
    }

    @Override
    public ArrayList<Float> giveWeeklyBalance() {
        ArrayList<Float> income = giveWeeklyData(1);
        ArrayList<Float> expense = giveWeeklyData(-1);
        ArrayList<Float> endResult = new ArrayList<>();
        for(int i = 0; i < income.size(); i++){
            if(i==0)
                endResult.add(income.get(i)-expense.get(i));
            else
                endResult.add(endResult.get(i-1)+income.get(i)-expense.get(i));
        }
        return  endResult;
    }

    @Override
    public ArrayList<Float> giveMonthlyBalance() {
        ArrayList<Float> income = giveMonthlyData(1);
        ArrayList<Float> expense = giveMonthlyData(-1);
        ArrayList<Float> endResult = new ArrayList<>();
        for(int i = 0; i < income.size(); i++){
            if(i==0)
                endResult.add(income.get(i)-expense.get(i));
            else
                endResult.add(endResult.get(i-1)+income.get(i)-expense.get(i));
        }
        return  endResult;
    }

    @Override
    public void calculate(String s) {
        new TLI(this).execute(new String[]{"GETALL"});
    }

    @Override
    public void onDone(ArrayList<Transaction> results, Account account) {
        trans=results;
        view.show(results);
    }

    @Override
    public void saveCurrentState(ArrayList<Transaction> results, Account account) {

    }
}
