package com.example.spirala1.detail.graph;

import com.example.spirala1.data.Transaction;

import java.util.ArrayList;

public interface IGraphPresenter {
    ArrayList<Float> giveDailyData(int mode);
    ArrayList<Float> giveMonthlyData(int mode);
    ArrayList<Float> giveWeeklyData(int mode);
    ArrayList<Float> giveDailyBalance();
    ArrayList<Float> giveWeeklyBalance();
    ArrayList<Float> giveMonthlyBalance();

    void calculate(String s);
}
