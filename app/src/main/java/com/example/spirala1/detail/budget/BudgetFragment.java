package com.example.spirala1.detail.budget;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.spirala1.R;
import com.example.spirala1.data.Account;

public class BudgetFragment extends Fragment implements IBudgetView{
    EditText balance,totallimit,monthlylimit;
    View fragmentView;
    Button save;
    private IBudgetPresenter presenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    IBudgetPresenter getPresenter(){
        if(presenter==null)presenter = new BudgetPresenter(this,getActivity());
        return presenter;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentView = inflater.inflate(R.layout.fragment_budget,container,false);

        balance = fragmentView.findViewById(R.id.budget);
        totallimit = fragmentView.findViewById(R.id.totallimit);
        monthlylimit = fragmentView.findViewById(R.id.monthlylimit);
        save = fragmentView.findViewById(R.id.save);
        getPresenter();

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPresenter().save(Double.parseDouble(balance.getText().toString()),Double.parseDouble(totallimit.getText().toString()),Double.parseDouble(monthlylimit.getText().toString()));
            }
        });


        return fragmentView;
    }

    @Override
    public void updateAccount(Account account) {
        balance.setText(account.getBalance()+"");
        totallimit.setText(account.getTotalLimit()+"");
        monthlylimit.setText(account.getMonthLimit()+"");
    }
}
