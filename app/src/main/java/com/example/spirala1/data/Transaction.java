package com.example.spirala1.data;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Calendar;
import java.util.Date;

public class Transaction implements Parcelable {
    private Calendar date = Calendar.getInstance();
    private double amount=0.;
    private String title="";
    private Payment type=Payment.REGULARPAYMENT;
    private String itemDescription="";
    private int transactionInterval=0;
    private Calendar endDate = Calendar.getInstance();
    private Integer id;
    private boolean obrisana = false;

    public Transaction(Date prvi, Date drugi, String title, double amount, Payment type, String desc, int interval){
        date.setTime(prvi);
        date.set(Calendar.HOUR_OF_DAY, 0);
        date.set(Calendar.MINUTE, 0);
        date.set(Calendar.SECOND, 0);
        date.set(Calendar.MILLISECOND, 0);
        endDate.setTime(drugi);
        endDate.set(Calendar.HOUR_OF_DAY, 0);
        endDate.set(Calendar.MINUTE, 0);
        endDate.set(Calendar.SECOND, 0);
        endDate.set(Calendar.MILLISECOND, 0);
        this.title = title;
        this.amount = amount;
        this.type=type;
        itemDescription=desc;
        transactionInterval = interval;
        id = null;
    }

    public Transaction(Date prvi, Date drugi, String title, double amount, Payment type, String desc, int interval, int identif){
        id = identif;
        date.setTime(prvi);
        date.set(Calendar.HOUR_OF_DAY, 0);
        date.set(Calendar.MINUTE, 0);
        date.set(Calendar.SECOND, 0);
        date.set(Calendar.MILLISECOND, 0);
        endDate.setTime(drugi);
        endDate.set(Calendar.HOUR_OF_DAY, 0);
        endDate.set(Calendar.MINUTE, 0);
        endDate.set(Calendar.SECOND, 0);
        endDate.set(Calendar.MILLISECOND, 0);
        this.title = title;
        this.amount = amount;
        this.type=type;
        itemDescription=desc;
        transactionInterval = interval;
        id = null;
    }

    public Transaction(int y, int m, int d, double amount, String title, Payment type, String itemDescription, Integer transactionInterval, Integer ey,Integer em,Integer ed) {
        date.set(Calendar.YEAR,y);
        date.set(Calendar.MONTH,m-1);
        date.set(Calendar.DAY_OF_MONTH,d);
        date.set(Calendar.HOUR_OF_DAY, 0);
        date.set(Calendar.MINUTE, 0);
        date.set(Calendar.SECOND, 0);
        date.set(Calendar.MILLISECOND, 0);
        this.amount = amount;
        this.title = title;
        this.type = type;
        if(type.equals(Payment.INDIVIDUALINCOME)||type.equals(Payment.REGULARINCOME)){
            this.itemDescription = "";
        }
        else{
            this.itemDescription = itemDescription;
        }
        if(type.equals(Payment.REGULARINCOME)||type.equals(Payment.REGULARPAYMENT)){
            this.transactionInterval = transactionInterval;
            endDate.set(Calendar.YEAR,ey);
            endDate.set(Calendar.MONTH,em);
            endDate.set(Calendar.DAY_OF_MONTH,ed);
        }
        else{
            transactionInterval = 0;
            endDate = date;
        }
        endDate.set(Calendar.HOUR_OF_DAY, 0);
        endDate.set(Calendar.MINUTE, 0);
        endDate.set(Calendar.SECOND, 0);
        endDate.set(Calendar.MILLISECOND, 0);
        id = null;
    }
    public Transaction(int y, int m, int d, double amount, String title, Payment type, String itemDescription, Integer transactionInterval, Integer ey,Integer em,Integer ed, Integer identification) {
        id = identification;
        date.set(Calendar.YEAR,y);
        date.set(Calendar.MONTH,m-1);
        date.set(Calendar.DAY_OF_MONTH,d);
        date.set(Calendar.HOUR_OF_DAY, 0);
        date.set(Calendar.MINUTE, 0);
        date.set(Calendar.SECOND, 0);
        date.set(Calendar.MILLISECOND, 0);
        this.amount = amount;
        this.title = title;
        this.type = type;
        if(type.equals(Payment.INDIVIDUALINCOME)||type.equals(Payment.REGULARINCOME)){
            this.itemDescription = "";
        }
        else{
            this.itemDescription = itemDescription;
        }
        if(type.equals(Payment.REGULARINCOME)||type.equals(Payment.REGULARPAYMENT)){
            this.transactionInterval = transactionInterval;
            endDate.set(Calendar.YEAR,ey);
            endDate.set(Calendar.MONTH,em);
            endDate.set(Calendar.DAY_OF_MONTH,ed);
        }
        else{
            transactionInterval = 0;
            endDate = date;
        }
        endDate.set(Calendar.HOUR_OF_DAY, 0);
        endDate.set(Calendar.MINUTE, 0);
        endDate.set(Calendar.SECOND, 0);
        endDate.set(Calendar.MILLISECOND, 0);
    }

    public Transaction() {
        amount = -1;
        type = Payment.INDIVIDUALINCOME;
        title = "ERROR";
        itemDescription="ERROR";
        transactionInterval=-1;
    }

    protected Transaction(Parcel in) {
        amount = in.readDouble();
        title = in.readString();
        itemDescription = in.readString();
        transactionInterval = in.readInt();
        type = (Payment)in.readSerializable();
        long temp = in.readLong();
        date.setTimeInMillis(temp);
        temp = in.readLong();
        endDate.setTimeInMillis(temp);
    }

    public static final Creator<Transaction> CREATOR = new Creator<Transaction>() {
        @Override
        public Transaction createFromParcel(Parcel in) {
            return new Transaction(in);
        }

        @Override
        public Transaction[] newArray(int size) {
            return new Transaction[size];
        }
    };

    public Calendar getDate() {
        return date;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Payment getType() {
        return type;
    }

    public void setType(Payment type) {
        this.type = type;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public boolean isIncomeType(){
        return type.equals(Payment.REGULARINCOME)||type.equals(Payment.INDIVIDUALINCOME);
    }
    public boolean isRegularType(){
        return type.equals(Payment.REGULARINCOME)||type.equals(Payment.REGULARPAYMENT);
    }

    public int getTransactionInterval() {
        return transactionInterval;
    }

    public void setTransactionInterval(int transactionInterval) {
        this.transactionInterval = transactionInterval;
    }

    public boolean isObrisana(){return obrisana;}
    public void setObrisana(boolean value){obrisana = value;}

    public Calendar getEndDate() {
        return endDate;
    }

    public void setEndDate(Calendar endDate) {
        this.endDate = endDate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(amount);
        dest.writeString(title);
        dest.writeString(itemDescription);
        dest.writeInt(transactionInterval);
        dest.writeSerializable(type);
        dest.writeLong(date.getTimeInMillis());
        dest.writeLong(endDate.getTimeInMillis());
    }
}
