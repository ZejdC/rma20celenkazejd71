package com.example.spirala1.data;

import java.util.ArrayList;
import java.util.List;

public class Model {
    public static List<Transaction> transactions = new ArrayList<Transaction>(){
        {
            add(new Transaction(2020,3,23,50,"Rata kredita", Payment.REGULARPAYMENT,"Za banku",60,2021,5,1));
            add(new Transaction(2020,4,2,200,"Režije",Payment.REGULARPAYMENT,"Za banku",31,2025,5,1));
            add(new Transaction(2020,1,30,1200,"Plata",Payment.REGULARINCOME,"Za banku",31,2025,5,1));
            add(new Transaction(2020,6,25,50,"Kupovina namirnica",Payment.PURCHASE,"Za banku",31,2025,5,1));
            add(new Transaction(2020,3,13,50,"Poklon",Payment.INDIVIDUALPAYMENT,"Za banku",31,2025,5,1));
            add(new Transaction(2020,4,17,50,"Tiket",Payment.INDIVIDUALINCOME,"Za banku",31,2025,5,1));
            add(new Transaction(2020,4,5,2000,"Utaja poreza",Payment.REGULARINCOME,"Za banku",90,2025,5,1));
            add(new Transaction(2020,5,1,50,"Restoran",Payment.INDIVIDUALPAYMENT,"Za banku",31,2025,5,1));
            add(new Transaction(2020,8,9,50,"Utakmica",Payment.INDIVIDUALPAYMENT,"Za banku",31,2025,5,1));
            add(new Transaction(2020,12,23,50,"Za novu godinu",Payment.INDIVIDUALPAYMENT,"Za banku",31,2025,5,1));
            add(new Transaction(2020,7,9,50,"Poklon",Payment.PURCHASE,"Za banku",31,2025,5,1));
            add(new Transaction(2020,2,28,50,"Sat",Payment.PURCHASE,"Za banku",31,2025,5,1));
            add(new Transaction(2020,3,14,50,"Košulja",Payment.PURCHASE,"Za banku",31,2025,5,1));
            add(new Transaction(2020,3,12,50,"Alati",Payment.PURCHASE,"Za banku",31,2025,5,1));
            add(new Transaction(2020,6,2,50,"Putovanje",Payment.INDIVIDUALPAYMENT,"Za banku",31,2025,5,1));
            add(new Transaction(2020,9,3,100,"Raspberry PI",Payment.INDIVIDUALPAYMENT,"Za banku",31,2025,5,1));
            add(new Transaction(2020,10,4,50,"Zamjena za nesto sa profila uz doplatu",Payment.INDIVIDUALINCOME,"Za banku",31,2025,5,1));
            add(new Transaction(2020,11,19,50,"Online instrukcije",Payment.INDIVIDUALINCOME,"Za banku",31,2025,5,1));
            add(new Transaction(2020,11,21,50,"Bajram banka",Payment.INDIVIDUALINCOME,"Za banku",31,2025,5,1));
            add(new Transaction(2020,4,23,50,"Školarina",Payment.INDIVIDUALPAYMENT,"Za banku",31,2025,5,1));
        }
    };
    public static Account account = new Account(20000,10000,2000);
}
