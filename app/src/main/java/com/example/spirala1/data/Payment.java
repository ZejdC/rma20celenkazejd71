package com.example.spirala1.data;

public enum Payment {
    INDIVIDUALPAYMENT{
        @Override
        public String toString(){
            return "Individual payment";
        }
    },
    REGULARPAYMENT{
        @Override
        public String toString(){
            return "Regular payment";
        }
    },
    PURCHASE{
        @Override
        public String toString(){
            return "Purchase";
        }
    },
    INDIVIDUALINCOME{
        @Override
        public String toString(){
            return "Individual income";
        }
    },
    REGULARINCOME{
        @Override
        public String toString(){
            return "Regular income";
        }
    }
}
