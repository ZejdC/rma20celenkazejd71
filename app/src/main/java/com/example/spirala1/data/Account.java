package com.example.spirala1.data;

import android.os.Parcel;
import android.os.Parcelable;

public class Account implements Parcelable {
    private double balance;
    private double totalLimit;
    private double monthLimit;

    public Account(double balance, double totalLimit, double monthLimit) {
        this.balance = balance;
        this.totalLimit = totalLimit;
        this.monthLimit = monthLimit;
    }

    public Account() {
        balance = 0;
        totalLimit=1000;
        monthLimit=100;
    }

    protected Account(Parcel in) {
        balance = in.readDouble();
        totalLimit = in.readDouble();
        monthLimit = in.readDouble();
    }

    public static final Creator<Account> CREATOR = new Creator<Account>() {
        @Override
        public Account createFromParcel(Parcel in) {
            return new Account(in);
        }

        @Override
        public Account[] newArray(int size) {
            return new Account[size];
        }
    };

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public double getTotalLimit() {
        return totalLimit;
    }

    public void setTotalLimit(double totalLimit) {
        this.totalLimit = totalLimit;
    }

    public double getMonthLimit() {
        return monthLimit;
    }

    public void setMonthLimit(double monthLimit) {
        this.monthLimit = monthLimit;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(balance);
        dest.writeDouble(totalLimit);
        dest.writeDouble(monthLimit);
    }
}
