package com.example.spirala1.list;

import com.example.spirala1.data.Account;
import com.example.spirala1.data.Transaction;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public interface ITransactionListView {
    void showInList(List<Transaction> lista);
    void updateAccount(String global, String balance);
    Calendar getCurrentDate();
    List<Transaction> getCurrentList();
    Account getCurrentAccount();
    void saveCurrentState(List<Transaction> lista, Account account);
    Account getSavedAccount();
    void setAccount(Account account);
}
