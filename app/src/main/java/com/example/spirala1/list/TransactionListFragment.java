package com.example.spirala1.list;

import android.content.Context;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.spirala1.data.Account;
import com.example.spirala1.R;
import com.example.spirala1.data.Payment;
import com.example.spirala1.data.Transaction;
import com.example.spirala1.util.MyArrayAdapter;
import com.example.spirala1.util.MySpinnerAdapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

public class TransactionListFragment extends Fragment implements ITransactionListView {
    View fragmentView;
    TextView global;
    TextView limit;
    Spinner filter;
    Spinner sorter;
    Button lbutton;
    Button rbutton;
    TextView date;
    ListView lista;
    Button add;
    String[] sortovi = {"Sort by", "Price - Ascending", "Price - Descending", "Title - Ascending",
            "Title - Descending", "Date - Ascending", "Date - Descending"};
    String[] filteri = {"Filter by", "Individual payment", "Regular payment", "Purchase",
            "Individual income", "Regular income"};
    private Calendar currentDate;
    private int pos=-1;
    private List<Transaction> lis;
    private ITransactionListPresenter presenter;
    private Account account;
    private Account savedAccount;
    private List<Transaction> savedList;

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
    public ITransactionListPresenter getPresenter(){
        if(presenter==null){
            presenter = new TransactionListPresenter(this,getActivity());
        }
        return presenter;
    }

    public interface OnItemClick {
        public void onItemClicked(Transaction transaction, Account account);
        public void onAddButtonClicked(Account account);
    }

    private OnItemClick onItemClick;

    public Account getAccount(){
        return account;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
    }



    private String[] getFilterStringArray(String id, String month, String year, String sort){
        return new String[]{"GETALLFILTER",id+"",month,year,sort};
    }

    private String getMonth(){
        return (new SimpleDateFormat("MM").format(currentDate.getTime()));
    }
    private String getYear(){
        return (new SimpleDateFormat("YYYY").format(currentDate.getTime()));
    }

    private void updateListToCurrentDate(){
        getPresenter().filter(getFilterStringArray("null",getMonth(),getYear(),"null"));
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentView = inflater.inflate(R.layout.fragment_list, container, false);

        date = fragmentView.findViewById(R.id.monthTxt);
        global = fragmentView.findViewById(R.id.globalTxt);
        limit = fragmentView.findViewById(R.id.limitTxt);
        filter = fragmentView.findViewById(R.id.filterSpinner);
        sorter = fragmentView.findViewById(R.id.sortSpinner);
        lbutton = fragmentView.findViewById(R.id.btnLeft);
        rbutton = fragmentView.findViewById(R.id.btnRight);
        lista = fragmentView.findViewById(R.id.list);
        add = fragmentView.findViewById(R.id.btnAdd);
        currentDate = Calendar.getInstance();

        onItemClick = (OnItemClick) getActivity();



        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item, sortovi);
        adapter.setDropDownViewResource(android.R.layout.select_dialog_singlechoice);

        sorter.setAdapter(adapter);
        sorter.setSelection(0,false);

        MySpinnerAdapter adapter1 = new MySpinnerAdapter(getActivity(), R.layout.spinnerelement, R.id.naziv, Arrays.asList(filteri));
        adapter1.setDropDownViewResource(android.R.layout.select_dialog_singlechoice);

        filter.setAdapter(adapter1);
        filter.setSelection(0,false);

        date.setText((new SimpleDateFormat("MMMM")).format(currentDate.getTime()) + ", " + (new SimpleDateFormat("YYYY").format(currentDate.getTime())));
        rbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pos=-1;
                currentDate.add(Calendar.MONTH, 1);
                lista.setAdapter(null);
                List<Transaction> list = new ArrayList<>();
                list.add(new Transaction(2000,1,1,0.,"Loading...",Payment.PURCHASE,"loading",0,2000,1,1));
                MyArrayAdapter maa = new MyArrayAdapter(getContext(), R.layout.listelement, R.id.naziv, list);
                lista.setAdapter(maa);
                lista.setEnabled(false);
                date.setText((new SimpleDateFormat("MMMM")).format(currentDate.getTime()) + ", " + (new SimpleDateFormat("YYYY").format(currentDate.getTime())));
                if(isNetworkAvailable())
                    getPresenter().filter(new String[]{"GETALLFILTER",(String)filter.getSelectedItem(),getMonth(),getYear(), (String) sorter.getSelectedItem()});
                else
                    getPresenter().showTransactionsOnCurrentDate(savedList);
            }
        });
        lbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pos=-1;
                currentDate.add(Calendar.MONTH, -1);
                lista.setAdapter(null);
                List<Transaction> list = new ArrayList<>();
                list.add(new Transaction(2000,1,1,0.,"Loading...", Payment.PURCHASE,"loading",0,2000,1,1));
                MyArrayAdapter maa = new MyArrayAdapter(getContext(), R.layout.listelement, R.id.naziv, list);
                lista.setAdapter(maa);
                lista.setEnabled(false);
                date.setText((new SimpleDateFormat("MMMM")).format(currentDate.getTime()) + ", " + (new SimpleDateFormat("YYYY").format(currentDate.getTime())));
                if(isNetworkAvailable())
                    getPresenter().filter(new String[]{"GETALLFILTER",(String)filter.getSelectedItem(),getMonth(),getYear(), (String) sorter.getSelectedItem()});
                else
                    getPresenter().showTransactionsOnCurrentDate(savedList);
            }
        });

        filter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(presenter!=null && isNetworkAvailable()) {
                    String s = (String) parent.getItemAtPosition(position);
                    lista.setAdapter(null);
                    List<Transaction> list = new ArrayList<>();
                    list.add(new Transaction(2000, 1, 1, 0., "Loading...", Payment.PURCHASE, "loading", 0, 2000, 1, 1));
                    MyArrayAdapter maa = new MyArrayAdapter(getContext(), R.layout.listelement, R.id.naziv, list);
                    lista.setAdapter(maa);
                    lista.setEnabled(false);
                    getPresenter().filter(new String[]{"GETALLFILTER", (String) filter.getSelectedItem(), getMonth(), getYear(), (String) sorter.getSelectedItem()});
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                if(presenter!=null && isNetworkAvailable()) {
                    lista.setAdapter(null);
                    List<Transaction> list = new ArrayList<>();
                    list.add(new Transaction(2000, 1, 1, 0., "Loading...", Payment.PURCHASE, "loading", 0, 2000, 1, 1));
                    MyArrayAdapter maa = new MyArrayAdapter(getContext(), R.layout.listelement, R.id.naziv, list);
                    lista.setAdapter(maa);
                    lista.setEnabled(false);
                    getPresenter().filter(new String[]{"GETALLFILTER", (String) filter.getSelectedItem(), getMonth(), getYear(), (String) sorter.getSelectedItem()});
                }
            }
        });
        sorter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(presenter!=null && isNetworkAvailable()) {
                    lista.setAdapter(null);
                    List<Transaction> list = new ArrayList<>();
                    list.add(new Transaction(2000, 1, 1, 0., "Loading...", Payment.PURCHASE, "loading", 0, 2000, 1, 1));
                    MyArrayAdapter maa = new MyArrayAdapter(getContext(), R.layout.listelement, R.id.naziv, list);
                    lista.setAdapter(maa);
                    lista.setEnabled(false);
                    getPresenter().filter(new String[]{"GETALLFILTER", (String) filter.getSelectedItem(), getMonth(), getYear(), (String) sorter.getSelectedItem()});
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(position!=pos) {
                    pos=position;
                    Transaction t = (Transaction) parent.getItemAtPosition(position);
                    getPresenter().prepareToChange(t);
                    int wanted = position - lista.getFirstVisiblePosition();
                    for (int i = 0; i < lista.getChildCount(); i++) {
                        lista.getChildAt(i).setBackgroundColor(Color.TRANSPARENT);
                    }
                    lista.getChildAt(wanted).setBackgroundColor(Color.GREEN);
                    if(isNetworkAvailable())
                        onItemClick.onItemClicked(t, getCurrentAccount());
                    else {
                        getPresenter().showTransactionsOnCurrentDate(savedList);
                        onItemClick.onItemClicked(t, savedAccount);
                    }
                }else{
                    pos=-1;
                    int wanted = position - lista.getFirstVisiblePosition();
                    lista.getChildAt(wanted).setBackgroundColor(Color.TRANSPARENT);
                    if(isNetworkAvailable())
                        onItemClick.onItemClicked((Transaction) parent.getItemAtPosition(position), getCurrentAccount());
                    else
                        onItemClick.onItemClicked((Transaction) parent.getItemAtPosition(position), savedAccount);
                }
            }
        });
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isNetworkAvailable())
                    onItemClick.onAddButtonClicked(getCurrentAccount());
                else
                    onItemClick.onAddButtonClicked(savedAccount);
            }
        });
        if(presenter!=null){
            if(isNetworkAvailable()) {
                getPresenter().filter(new String[]{"GETALLFILTER", (String) filter.getSelectedItem(), getMonth(), getYear(), (String) sorter.getSelectedItem()});
                getPresenter().getAccount();
            }else{
                getPresenter().showTransactionsOnCurrentDate(savedList);
                updateAccount("Global amount: " + savedAccount.getBalance(), "Limit: " + savedAccount.getTotalLimit());
            }
        }
        getPresenter();
        return fragmentView;
    }

    @Override
    public void showInList(List<Transaction> list) {
        lista.setEnabled(true);
        lis = list;
        lista.setAdapter(null);
        MyArrayAdapter maa = new MyArrayAdapter(getContext(), R.layout.listelement, R.id.naziv, list);
        lista.setAdapter(maa);
    }

    @Override
    public void updateAccount(String gl, String lim) {
        global.setText(gl);
        limit.setText(lim);
    }

    @Override
    public Calendar getCurrentDate() {
        return currentDate;
    }

    @Override
    public List<Transaction> getCurrentList() {
        return savedList;
    }

    @Override
    public Account getCurrentAccount() {
        return account;
    }
    //CUVA NAJAKTUELNIJE PODATKE
    @Override
    public void saveCurrentState(List<Transaction> lista, Account account) {
        if(lista != null && lista.size()>0)
        savedList = lista;
        if(account!=null)
        savedAccount = account;
        //Toast.makeText(getContext(),"Spaseno lokalno!",Toast.LENGTH_SHORT).show();
    }

    @Override
    public Account getSavedAccount() {
        return savedAccount;
    }

    @Override
    public void setAccount(Account account) {
        this.account = account;
    }

    @Override
    public void onResume() {
        if(getArguments()!=null){
            if(isNetworkAvailable()) {
                if (getArguments().containsKey("add")) {
                    getPresenter().addToTransactions(getArguments());
                    getArguments().clear();
                } else if (getArguments().containsKey("delete")) {
                    getPresenter().deletePrepared();
                    getArguments().clear();
                } else if (getArguments().containsKey("edit")) {
                    getPresenter().savePrepared(getArguments());
                    getArguments().clear();
                }
            }else{
                if (getArguments().containsKey("add")) {
                    getPresenter().addToTable("add",getArguments());
                    getArguments().clear();
                } else if (getArguments().containsKey("delete")) {
                    getPresenter().addToTable("delete",getArguments());
                    getArguments().clear();
                } else if (getArguments().containsKey("edit")) {
                    getPresenter().addToTable("edit",getArguments());
                    getArguments().clear();
                } else if (getArguments().containsKey("undo")){
                    getPresenter().undo(getArguments());
                    getArguments().clear();
                }
            }
        }
        super.onResume();
    }

}


