package com.example.spirala1.list;

import com.example.spirala1.data.Account;
import com.example.spirala1.data.Transaction;

import java.util.ArrayList;

public interface ITransactionListInteractor {
    ArrayList<Transaction> getTransactions();
    Account getAccount();
}
