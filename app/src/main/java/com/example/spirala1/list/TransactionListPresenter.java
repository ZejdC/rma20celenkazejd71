package com.example.spirala1.list;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.FragmentActivity;

import com.example.spirala1.data.Account;
import com.example.spirala1.data.Payment;
import com.example.spirala1.data.Transaction;
import com.example.spirala1.util.TransactionDBOpenHelper;

import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class TransactionListPresenter implements ITransactionListPresenter, TLI.interactorInterface{
    Context context;
    ITransactionListView view;
    Integer change = -1;
    private Transaction toChange = null;

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private Cursor getCursor(String table, Context context, String upit){
        ContentResolver cr = context.getApplicationContext().getContentResolver();
        String where = upit;
        String whereArgs[] = null;
        String order = null;
        String[] kolone = null;
        Uri adresa = null;
        switch (table){
            case "ADD":
                adresa = Uri.parse("content://spirala.provider.add/elements");
                kolone = new String[]{
                        TransactionDBOpenHelper.ID, TransactionDBOpenHelper.TITLE, TransactionDBOpenHelper.AMOUNT,
                        TransactionDBOpenHelper.TYPE, TransactionDBOpenHelper.DESCRIPTION, TransactionDBOpenHelper.INTERVAL,
                        TransactionDBOpenHelper.DATE, TransactionDBOpenHelper.ENDDATE};
                break;
            case "EDIT":
                adresa = Uri.parse("content://spirala.provider.edit/elements");
                kolone = new String[]{
                        TransactionDBOpenHelper.ID, TransactionDBOpenHelper.TITLE, TransactionDBOpenHelper.AMOUNT,
                        TransactionDBOpenHelper.TYPE, TransactionDBOpenHelper.DESCRIPTION, TransactionDBOpenHelper.INTERVAL,
                        TransactionDBOpenHelper.DATE, TransactionDBOpenHelper.ENDDATE};
                break;
            case "DELETE":
                adresa = Uri.parse("content://spirala.provider.delete/elements");
                kolone = new String[]{
                        TransactionDBOpenHelper.ID, TransactionDBOpenHelper.TITLE, TransactionDBOpenHelper.AMOUNT,
                        TransactionDBOpenHelper.TYPE, TransactionDBOpenHelper.DESCRIPTION, TransactionDBOpenHelper.INTERVAL,
                        TransactionDBOpenHelper.DATE, TransactionDBOpenHelper.ENDDATE
                };
                break;
            case "ACCOUNT":
                adresa = Uri.parse("content://spirala.provider.account/elements");
                kolone = new String[]{
                        TransactionDBOpenHelper.BUDGET, TransactionDBOpenHelper.MONTHLIMIT, TransactionDBOpenHelper.TOTALLIMIT
                };
                break;
            default:
                return null;
        };

        Cursor cur = cr.query(adresa,kolone,where,whereArgs,order);
        return cur;
    }

    private String getCursorValue(Cursor c, String s){
        return c.getString(c.getColumnIndexOrThrow(s));
    }

    public TransactionListPresenter(ITransactionListView transactionListFragment, Context activity) {
        view = transactionListFragment;
        context = activity;
        List<Transaction> lista = new ArrayList<>();
        Account account = null;
        if(isNetworkAvailable()) {
            new TLI(this).execute(new String[]{"GETALL"});
            new TLI(this).execute(new String[]{"GETALLFILTER", "null", getMonth(view.getCurrentDate().getTime()), getYear(view.getCurrentDate().getTime()), "null"});
            new TLI(this).execute(new String[]{"GETACC"});
            return;
        }else{
            try {
                Cursor c = getCursor("ADD", context, null);
                while (c.moveToNext()) {
                    Integer id = Integer.valueOf(getCursorValue(c, TransactionDBOpenHelper.ID));
                    String title = getCursorValue(c, TransactionDBOpenHelper.TITLE);
                    Double amount = Double.valueOf(getCursorValue(c, TransactionDBOpenHelper.AMOUNT));
                    Payment payment = getPayment(getCursorValue(c, TransactionDBOpenHelper.TYPE));
                    String desciption = getCursorValue(c, TransactionDBOpenHelper.DESCRIPTION);
                    Integer interval = Integer.valueOf(getCursorValue(c, TransactionDBOpenHelper.INTERVAL));
                    String date = getCursorValue(c, TransactionDBOpenHelper.DATE);
                    String enddate = getCursorValue(c, TransactionDBOpenHelper.ENDDATE);
                    String[] pocetak = date.split("-");
                    String[] kraj = enddate.split("-");
                    String[] ddan = pocetak[2].split("T");
                    String[] edan = kraj[2].split("T");
                    Transaction t = new Transaction(Integer.valueOf(pocetak[0]), Integer.valueOf(pocetak[1]), Integer.valueOf(ddan[0]),
                            amount, title, payment, desciption, interval, Integer.valueOf(kraj[0]), Integer.valueOf(kraj[1]), Integer.valueOf(edan[0]), id);
                    lista.add(t);
                }
                c.close();
                c = getCursor("EDIT", context, null);
                while (c.moveToNext()) {
                    Integer id = Integer.valueOf(getCursorValue(c, TransactionDBOpenHelper.ID));
                    String title = getCursorValue(c, TransactionDBOpenHelper.TITLE);
                    Double amount = Double.valueOf(getCursorValue(c, TransactionDBOpenHelper.AMOUNT));
                    Payment payment = getPayment(getCursorValue(c, TransactionDBOpenHelper.TYPE));
                    String desciption = getCursorValue(c, TransactionDBOpenHelper.DESCRIPTION);
                    Integer interval = Integer.valueOf(getCursorValue(c, TransactionDBOpenHelper.INTERVAL));
                    String date = getCursorValue(c, TransactionDBOpenHelper.DATE);
                    String enddate = getCursorValue(c, TransactionDBOpenHelper.ENDDATE);
                    String[] pocetak = date.split("-");
                    String[] kraj = enddate.split("-");
                    String[] ddan = pocetak[2].split("T");
                    String[] edan = kraj[2].split("T");
                    Transaction t = new Transaction(Integer.valueOf(pocetak[0]), Integer.valueOf(pocetak[1]), Integer.valueOf(ddan[0]),
                            amount, title, payment, desciption, interval, Integer.valueOf(kraj[0]), Integer.valueOf(kraj[1]), Integer.valueOf(edan[0]), id);
                    lista.add(t);
                }
                c.close();
                c = getCursor("DELETE", context, null);
                while (c.moveToNext()) {
                    Integer id = Integer.valueOf(getCursorValue(c, TransactionDBOpenHelper.ID));
                    String title = getCursorValue(c, TransactionDBOpenHelper.TITLE);
                    Double amount = Double.valueOf(getCursorValue(c, TransactionDBOpenHelper.AMOUNT));
                    Payment payment = getPayment(getCursorValue(c, TransactionDBOpenHelper.TYPE));
                    String desciption = getCursorValue(c, TransactionDBOpenHelper.DESCRIPTION);
                    Integer interval = Integer.valueOf(getCursorValue(c, TransactionDBOpenHelper.INTERVAL));
                    String date = getCursorValue(c, TransactionDBOpenHelper.DATE);
                    String enddate = getCursorValue(c, TransactionDBOpenHelper.ENDDATE);
                    String[] pocetak = date.split("-");
                    String[] kraj = enddate.split("-");
                    String[] ddan = pocetak[2].split("T");
                    String[] edan = kraj[2].split("T");
                    Transaction t = new Transaction(Integer.valueOf(pocetak[0]), Integer.valueOf(pocetak[1]), Integer.valueOf(ddan[0]),
                            amount, title, payment, desciption, interval, Integer.valueOf(kraj[0]), Integer.valueOf(kraj[1]), Integer.valueOf(edan[0]), id);
                    t.setObrisana(true);
                    lista.add(t);
                }
                c.close();
                c = getCursor("ACCOUNT", context, null);
                while (c.moveToNext()) {
                    Double budget = Double.valueOf(getCursorValue(c, TransactionDBOpenHelper.BUDGET));
                    Double month = Double.valueOf(getCursorValue(c, TransactionDBOpenHelper.MONTHLIMIT));
                    Double total = Double.valueOf(getCursorValue(c, TransactionDBOpenHelper.TOTALLIMIT));
                    account = new Account(budget, total, month);
                }
                c.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        view.saveCurrentState(lista,account);
        showTransactionsOnCurrentDate(lista);
        view.updateAccount("Global amount: " + account.getBalance(), "Limit: " + account.getTotalLimit());
    }
    private String getMonth(Date date){
        return (new SimpleDateFormat("MM")).format(date);
    }
    private String getYear(Date date){
        return (new SimpleDateFormat("YYYY")).format(date);
    }

    public boolean sameMonthAndYear(Calendar a, Calendar b){
        SimpleDateFormat month = new SimpleDateFormat("MMMM");
        SimpleDateFormat year = new SimpleDateFormat("YYYY");
        return month.format(a.getTime()).equals(month.format(b.getTime())) &&
                year.format(a.getTime()).equals(year.format(b.getTime()));
    }

    public void getAccount(){
        new TLI(this).execute(new String[]{"GETACC"});
    }

    @Override
    public void addToTable(String table, Bundle data) {
        Transaction t = null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.000");
        ContentValues cv = new ContentValues();
        Cursor c;
        double balance;
        try {
            switch (table) {
                case "add":
                    t = data.getParcelable("transaction");
                    cv.put(TransactionDBOpenHelper.TITLE, t.getTitle());
                    cv.put(TransactionDBOpenHelper.AMOUNT, t.getAmount());
                    cv.put(TransactionDBOpenHelper.TYPE, t.getType().toString());
                    cv.put(TransactionDBOpenHelper.DESCRIPTION, t.getItemDescription());
                    cv.put(TransactionDBOpenHelper.INTERVAL, t.getTransactionInterval());
                    cv.put(TransactionDBOpenHelper.DATE, sdf.format(t.getDate().getTime()));
                    cv.put(TransactionDBOpenHelper.ENDDATE, sdf.format(t.getEndDate().getTime()));
                    context.getContentResolver().insert(Uri.parse("content://spirala.provider.add"), cv);
                    view.getCurrentList().add(t);
                    showTransactionsOnCurrentDate(view.getCurrentList());
                    if (t.getType().equals(Payment.INDIVIDUALINCOME) || t.getType().equals(Payment.REGULARINCOME)) {
                        balance = view.getSavedAccount().getBalance() + t.getAmount();
                    } else {
                        balance = view.getSavedAccount().getBalance() - t.getAmount();
                    }
                    cv.clear();
                    cv.put(TransactionDBOpenHelper.BUDGET, balance);
                    context.getContentResolver().update(Uri.parse("content://spirala.provider.account"), cv, null, null);
                    break;
                case "edit":
                    t = data.getParcelable("transaction");
                    //PROVJERA JESMO LI KLIKNULI NA TRANSAKCIJU U ADD TABELI (TJ. NIJE JOS NA SERVERU)
                    c = getCursor("ADD", context, TransactionDBOpenHelper.TITLE + "='" + toChange.getTitle() + "' AND " + TransactionDBOpenHelper.AMOUNT + "=" + toChange.getAmount());
                    if (c.moveToNext()) {
                        cv.put(TransactionDBOpenHelper.TITLE, t.getTitle());
                        cv.put(TransactionDBOpenHelper.AMOUNT, t.getAmount());
                        cv.put(TransactionDBOpenHelper.TYPE, t.getType().toString());
                        cv.put(TransactionDBOpenHelper.DESCRIPTION, t.getItemDescription());
                        cv.put(TransactionDBOpenHelper.INTERVAL, t.getTransactionInterval());
                        cv.put(TransactionDBOpenHelper.DATE, sdf.format(t.getDate().getTime()));
                        cv.put(TransactionDBOpenHelper.ENDDATE, sdf.format(t.getEndDate().getTime()));
                        context.getContentResolver().update(Uri.parse("content://spirala.provider.add"), cv,
                                TransactionDBOpenHelper.TITLE + "='" + toChange.getTitle() + "' AND " + TransactionDBOpenHelper.AMOUNT + "=" + toChange.getAmount(), null);
                        view.getCurrentList().set(view.getCurrentList().indexOf(toChange), t);
                        showTransactionsOnCurrentDate(view.getCurrentList());
                        balance = Double.parseDouble(data.getString("balance"));
                        cv.clear();
                        cv.put(TransactionDBOpenHelper.BUDGET, balance);
                        context.getContentResolver().update(Uri.parse("content://spirala.provider.account"), cv, null, null);
                        toChange = null;
                        change = -1;
                        break;
                    }
                    c = getCursor("EDIT", context, TransactionDBOpenHelper.TITLE + "='" + toChange.getTitle() + "' AND " + TransactionDBOpenHelper.AMOUNT + "=" + toChange.getAmount());
                    if (c.moveToNext()) {
                        cv.put(TransactionDBOpenHelper.TITLE, t.getTitle());
                        cv.put(TransactionDBOpenHelper.AMOUNT, t.getAmount());
                        cv.put(TransactionDBOpenHelper.TYPE, t.getType().toString());
                        cv.put(TransactionDBOpenHelper.DESCRIPTION, t.getItemDescription());
                        cv.put(TransactionDBOpenHelper.INTERVAL, t.getTransactionInterval());
                        cv.put(TransactionDBOpenHelper.DATE, sdf.format(t.getDate().getTime()));
                        cv.put(TransactionDBOpenHelper.ENDDATE, sdf.format(t.getEndDate().getTime()));
                        context.getContentResolver().update(Uri.parse("content://spirala.provider.edit"), cv,
                                TransactionDBOpenHelper.TITLE + "='" + toChange.getTitle() + "' AND " + TransactionDBOpenHelper.AMOUNT + "=" + toChange.getAmount(), null);
                        view.getCurrentList().set(view.getCurrentList().indexOf(toChange), t);
                        showTransactionsOnCurrentDate(view.getCurrentList());
                        balance = Double.parseDouble(data.getString("balance"));
                        cv.clear();
                        cv.put(TransactionDBOpenHelper.BUDGET, balance);
                        context.getContentResolver().update(Uri.parse("content://spirala.provider.account"), cv, null, null);
                        toChange = null;
                        change = -1;
                        break;
                    }
                    cv.put(TransactionDBOpenHelper.ID, change);
                    cv.put(TransactionDBOpenHelper.TITLE, t.getTitle());
                    cv.put(TransactionDBOpenHelper.AMOUNT, t.getAmount());
                    cv.put(TransactionDBOpenHelper.TYPE, t.getType().toString());
                    cv.put(TransactionDBOpenHelper.DESCRIPTION, t.getItemDescription());
                    cv.put(TransactionDBOpenHelper.INTERVAL, t.getTransactionInterval());
                    cv.put(TransactionDBOpenHelper.DATE, sdf.format(t.getDate().getTime()));
                    cv.put(TransactionDBOpenHelper.ENDDATE, sdf.format(t.getEndDate().getTime()));
                    context.getContentResolver().insert(Uri.parse("content://spirala.provider.edit"), cv);
                    view.getCurrentList().set(view.getCurrentList().indexOf(toChange), t);
                    showTransactionsOnCurrentDate(view.getCurrentList());
                    balance = Double.parseDouble(data.getString("balance"));
                    cv.clear();
                    cv.put(TransactionDBOpenHelper.BUDGET, balance);
                    context.getContentResolver().update(Uri.parse("content://spirala.provider.account"), cv, null, null);
                    toChange = null;
                    change = -1;
                    break;
                case "delete":
                    t = toChange;
                    //PROVJERA JESMO LI KLIKNULI NA TRANSAKCIJU U ADD TABELI (TJ. NIJE JOS NA SERVERU)
                    c = getCursor("ADD", context, TransactionDBOpenHelper.TITLE + "='" + toChange.getTitle() + "' AND " + TransactionDBOpenHelper.AMOUNT + "=" + toChange.getAmount());
                    if (c.moveToNext()) {
                        context.getContentResolver().delete(Uri.parse("content://spirala.provider.add"), TransactionDBOpenHelper.TITLE + "='" + toChange.getTitle() + "' AND " + TransactionDBOpenHelper.AMOUNT + "=" + toChange.getAmount(), null);
                    }
                    c = getCursor("EDIT", context, TransactionDBOpenHelper.TITLE + "='" + toChange.getTitle() + "' AND " + TransactionDBOpenHelper.AMOUNT + "=" + toChange.getAmount());
                    if (c.moveToNext()) {
                        context.getContentResolver().delete(Uri.parse("content://spirala.provider.edit"), TransactionDBOpenHelper.TITLE + "='" + toChange.getTitle() + "' AND " + TransactionDBOpenHelper.AMOUNT + "=" + toChange.getAmount(), null);
                    }
                    cv.put(TransactionDBOpenHelper.ID, t.getId());
                    cv.put(TransactionDBOpenHelper.TITLE, t.getTitle());
                    cv.put(TransactionDBOpenHelper.AMOUNT, t.getAmount());
                    cv.put(TransactionDBOpenHelper.TYPE, t.getType().toString());
                    cv.put(TransactionDBOpenHelper.DESCRIPTION, t.getItemDescription());
                    cv.put(TransactionDBOpenHelper.INTERVAL, t.getTransactionInterval());
                    cv.put(TransactionDBOpenHelper.DATE, sdf.format(t.getDate().getTime()));
                    cv.put(TransactionDBOpenHelper.ENDDATE, sdf.format(t.getEndDate().getTime()));
                    context.getContentResolver().insert(Uri.parse("content://spirala.provider.delete"), cv);
                    if (toChange.getType().equals(Payment.INDIVIDUALINCOME) || toChange.getType().equals(Payment.REGULARINCOME)) {
                        balance = view.getSavedAccount().getBalance() - toChange.getAmount();
                    } else {
                        balance = view.getSavedAccount().getBalance() + toChange.getAmount();
                    }
                    cv.clear();
                    cv.put(TransactionDBOpenHelper.BUDGET, balance);
                    context.getContentResolver().update(Uri.parse("content://spirala.provider.account"), cv, null, null);
                    view.getCurrentList().get(view.getCurrentList().indexOf(toChange)).setObrisana(true);
                    showTransactionsOnCurrentDate(view.getCurrentList());
                    toChange = null;
                    change = -1;
                    break;
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public void undo(Bundle arguments) {
        try {
            Transaction t = toChange;
            ContentValues cv = new ContentValues();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.000");
            Double balance;
            int id = change;
            if (id < 225) {
                context.getContentResolver().delete(Uri.parse("content://spirala.provider.delete"), TransactionDBOpenHelper.ID + "=" + change, null);
                cv.put(TransactionDBOpenHelper.ID, t.getId());
                cv.put(TransactionDBOpenHelper.TITLE, t.getTitle());
                cv.put(TransactionDBOpenHelper.AMOUNT, t.getAmount());
                cv.put(TransactionDBOpenHelper.TYPE, t.getType().toString());
                cv.put(TransactionDBOpenHelper.DESCRIPTION, t.getItemDescription());
                cv.put(TransactionDBOpenHelper.INTERVAL, t.getTransactionInterval());
                cv.put(TransactionDBOpenHelper.DATE, sdf.format(t.getDate().getTime()));
                cv.put(TransactionDBOpenHelper.ENDDATE, sdf.format(t.getEndDate().getTime()));
                context.getContentResolver().insert(Uri.parse("content://spirala.provider.add"), cv);
            } else {
                context.getContentResolver().delete(Uri.parse("content://spirala.provider.delete"), TransactionDBOpenHelper.ID + "=" + change, null);
                cv.put(TransactionDBOpenHelper.ID, t.getId());
                cv.put(TransactionDBOpenHelper.TITLE, t.getTitle());
                cv.put(TransactionDBOpenHelper.AMOUNT, t.getAmount());
                cv.put(TransactionDBOpenHelper.TYPE, t.getType().toString());
                cv.put(TransactionDBOpenHelper.DESCRIPTION, t.getItemDescription());
                cv.put(TransactionDBOpenHelper.INTERVAL, t.getTransactionInterval());
                cv.put(TransactionDBOpenHelper.DATE, sdf.format(t.getDate().getTime()));
                cv.put(TransactionDBOpenHelper.ENDDATE, sdf.format(t.getEndDate().getTime()));
                context.getContentResolver().insert(Uri.parse("content://spirala.provider.edit"), cv);
            }
            if(t.getType().equals(Payment.INDIVIDUALINCOME)||t.getType().equals(Payment.REGULARINCOME)){
                balance = view.getCurrentAccount().getBalance()+t.getAmount();
            }
            else{
                balance = view.getCurrentAccount().getBalance()-t.getAmount();
            }
            cv.clear();
            cv.put(TransactionDBOpenHelper.BUDGET, balance);
            context.getContentResolver().update(Uri.parse("content://spirala.provider.account"), cv, null, null);
            view.getCurrentList().get(view.getCurrentList().indexOf(toChange)).setObrisana(false);
            change = -1;
            toChange = null;
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public int timesBetweenStartAndEnd(Transaction t, Calendar end){
        if(!t.isRegularType())return 1;
        Calendar temp = (Calendar)t.getDate().clone();
        int brojac = 0;
        while(temp.before(end)){
            brojac++;
            temp.add(Calendar.DAY_OF_MONTH,t.getTransactionInterval());
        }
        return brojac;
    }

    public int timesToShow(Transaction t){
        if(!t.isRegularType()&&sameMonthAndYear(view.getCurrentDate(),t.getDate()))return 1;
        else if(!t.isRegularType())return 0;
        if(t.getEndDate().before(view.getCurrentDate()))return 0;
        Calendar tren = (Calendar) t.getDate().clone();
        do{
            tren.add(Calendar.DAY_OF_MONTH,t.getTransactionInterval());
            if(sameMonthAndYear(view.getCurrentDate(),tren))break;
        }while(tren.before(view.getCurrentDate()));
        int brojac = 0;
        while (sameMonthAndYear(view.getCurrentDate(),tren)){
            brojac++;
            tren.add(Calendar.DAY_OF_MONTH,t.getTransactionInterval());
        }
        return brojac;
    }

    public void updateListToCurrentDate(){
        new TLI((TLI.interactorInterface)this).execute(new String[]{"FILTER",});
    }

    public void showTransactionsOnCurrentDate(List<Transaction> list){
        List<Transaction> nova = new ArrayList<>();
        if(list==null){view.showInList(nova);return;}
        for(Transaction t: list){
            int x = timesToShow(t);
            if(x>0){
                while(x>0){
                    nova.add(t);
                    x--;
                }
            }
            else if(sameMonthAndYear(view.getCurrentDate(),t.getDate())){
                nova.add(t);
            }
        }
        view.showInList(nova);
    }

    public void prepareToChange(Transaction t) {
        toChange = t;
        change = t.getId();
    }

    public void savePrepared(Bundle data) {
        Transaction t = data.getParcelable("transaction");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.000");
        String[]argument = {"EDIT",t.getTitle(),t.getAmount()+"",getId(t.getType().toString())+"",t.getItemDescription(),t.getTransactionInterval()+"",sdf.format(t.getDate().getTime())+"Z",sdf.format(t.getEndDate().getTime())+"Z",toChange.getId()+""};
        change=-1;
        new TLI(this).execute(argument);
        double balance = Double.parseDouble(data.getString("balance"));
        new TLI(this).execute(new String[]{"EDITACC",balance+"","null","null"});
    }

    private Payment getPayment(String type) {
        switch (type){
            case "Individual payment":
                return Payment.INDIVIDUALPAYMENT;
            case "Regular payment":
                return Payment.REGULARPAYMENT;
            case "Purchase":
                return Payment.PURCHASE;
            case "Individual income":
                return Payment.INDIVIDUALINCOME;
            case "Regular income":
                return Payment.REGULARINCOME;
            default:
                return null;
        }
    }

    public void deletePrepared() {
        new TLI(this).execute(new String[]{"DELETE",toChange.getId()+""});
        double balance;
        if(toChange.getType().equals(Payment.INDIVIDUALINCOME)||toChange.getType().equals(Payment.REGULARINCOME)){
            balance = view.getCurrentAccount().getBalance()-toChange.getAmount();
        }
        else{
            balance = view.getCurrentAccount().getBalance()+toChange.getAmount();
        }
        new TLI(this).execute(new String[]{"EDITACC",balance+"","null","null"});
        toChange = null;

    }

    public void addToTransactions(Bundle data) {
        Transaction t = data.getParcelable("transaction");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.000");

        String[]argument = {"ADD",t.getTitle(),t.getAmount()+"",getId(t.getType().toString())+"",t.getItemDescription(),t.getTransactionInterval()+"",sdf.format(t.getDate().getTime())+"Z",sdf.format(t.getEndDate().getTime())+"Z"};
        new TLI((TLI.interactorInterface)this).execute(argument);
        double balance;
        if(t.getType().equals(Payment.INDIVIDUALINCOME)||t.getType().equals(Payment.REGULARINCOME)){
            balance = view.getCurrentAccount().getBalance()+t.getAmount();
        }
        else{
            balance = view.getCurrentAccount().getBalance()-t.getAmount();
        }
        new TLI(this).execute(new String[]{"EDITACC",balance+"","null","null"});
    }

    private Integer getId(String type){
        switch (type){
            case "Individual payment":
                return 5;
            case "Regular payment":
                return 1;
            case "Purchase":
                return 3;
            case "Individual income":
                return 4;
            case "Regular income":
                return 2;
            default:
                return null;
        }
    }

    @Override
    public void onDone(ArrayList<Transaction> results, Account account) {
        if(account==null) {
            view.showInList(results);
        }
        else if(results != null && results.size()>0){
            view.showInList(results);
            view.updateAccount("Global amount: " + account.getBalance(), "Limit: " + account.getTotalLimit());
            view.setAccount(account);
        }
        else{
            view.updateAccount("Global amount: " + account.getBalance(), "Limit: " + account.getTotalLimit());
            view.setAccount(account);
        }
        new TLI(this).execute(new String[]{"GETALL"});
    }

    @Override
    public void saveCurrentState(ArrayList<Transaction> results, Account account) {
        view.saveCurrentState(results,account);
    }

    //ORDER 0-GETALLFILTER 1-ID,2-MONTH,3-YEAR,4-SORT
    @Override
    public void filter(String[] strings){
        new TLI((TLI.interactorInterface)this).execute(strings);
    }


    public interface View {
        void showInList(List<Transaction> lista);
        void updateAccount(String global, String balance);
        Calendar getCurrentDate();
        List<Transaction> getCurrentList();
        Account getCurrentAccount();
        void saveCurrentState(List<Transaction> lista, Account account);
    }
}
