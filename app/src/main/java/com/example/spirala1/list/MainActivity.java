package com.example.spirala1.list;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.content.IntentFilter;
import android.os.Bundle;
import android.widget.FrameLayout;
import android.widget.Toast;
import android.widget.Toolbar;

import com.example.spirala1.data.Account;
import com.example.spirala1.detail.add.AddTransactionFragment;
import com.example.spirala1.detail.budget.BudgetFragment;
import com.example.spirala1.R;
import com.example.spirala1.data.Transaction;
import com.example.spirala1.detail.detail.TransactionDetailFragment;
import com.example.spirala1.detail.graph.GraphsFragment;
import com.example.spirala1.util.ConnectivityBroadcastReceiver;
import com.example.spirala1.util.OnSwipeTouchListener;
import com.example.spirala1.util.TransactionIntentService;
import com.example.spirala1.util.TransactionReceiver;

public class MainActivity extends AppCompatActivity implements TransactionListFragment.OnItemClick, AddTransactionFragment.OnItemClick,
        TransactionDetailFragment.OnItemClicked, OnSwipeTouchListener.onSwipeListener, TransactionReceiver.Receiver {
    private boolean twoPaneMode;
    Transaction last=new Transaction();
    OnSwipeTouchListener onSwipeTouchListener;
    private ConnectivityBroadcastReceiver receiver = new ConnectivityBroadcastReceiver();
    private IntentFilter filter = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("Personal finance");

        FragmentManager fragmentManager = getSupportFragmentManager();
        FrameLayout details = findViewById(R.id.transaction_detail);
        if (details != null) {
            twoPaneMode = true;
            AddTransactionFragment detailFragment=null;
            detailFragment = new AddTransactionFragment();
            fragmentManager.beginTransaction().
                        replace(R.id.transaction_detail,detailFragment)
                        .commit();
        } else {
            twoPaneMode = false;
        }
        Fragment listFragment =  fragmentManager.findFragmentById(R.id.transaction_list);
        if (listFragment==null){
            listFragment = new TransactionListFragment();
            fragmentManager.beginTransaction()
                    .replace(R.id.transaction_list,listFragment)
                    .commit();
        }
        else{
            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
        if(!twoPaneMode){
            onSwipeTouchListener = new OnSwipeTouchListener(this, findViewById(R.id.linearLayout));
        }
        else{
            onSwipeTouchListener = null;
        }
    }

    @Override
    public void onItemClicked(Transaction transaction, Account account) {
        if(!transaction.equals(last)) {
            last = transaction;
            TransactionDetailFragment tda = new TransactionDetailFragment();
            Bundle arguments = new Bundle();
            arguments.putParcelable("transaction",transaction);
            arguments.putParcelable("account",account);
            tda.setArguments(arguments);
            if(twoPaneMode){
                getSupportFragmentManager().beginTransaction().replace(R.id.transaction_detail,tda).commit();
            }else{
                getSupportFragmentManager().beginTransaction().replace(R.id.transaction_list,tda).addToBackStack(null).commit();
            }
        }else{
            last = new Transaction();
            AddTransactionFragment atf = new AddTransactionFragment();
            Bundle arguments = new Bundle();
            arguments.putParcelable("account",account);
            atf.setArguments(arguments);
            if(twoPaneMode){
                getSupportFragmentManager().beginTransaction().replace(R.id.transaction_detail,atf).commit();
            }else{
                getSupportFragmentManager().beginTransaction().replace(R.id.transaction_list,atf).addToBackStack(null).commit();
            }
        }
    }

    @Override
    public void onAddButtonClicked(Account account) {
        AddTransactionFragment atf = new AddTransactionFragment();
        Bundle arguments = new Bundle();
        arguments.putParcelable("account",account);
        atf.setArguments(arguments);
        TransactionListFragment tlf = (TransactionListFragment) getSupportFragmentManager().findFragmentById(R.id.transaction_list);
        if(twoPaneMode){
            getSupportFragmentManager().beginTransaction().replace(R.id.transaction_detail,atf).commit();
        }
        else{
            getSupportFragmentManager().beginTransaction().replace(R.id.transaction_list,atf).addToBackStack(null).commit();
        }
    }

    @Override
    public void onSaveClicked(Bundle bundle) {
        if(!twoPaneMode){
            getSupportFragmentManager().popBackStackImmediate();
        }
        TransactionListFragment tlf = (TransactionListFragment) getSupportFragmentManager().findFragmentById(R.id.transaction_list);
        tlf.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().detach(tlf).commitNow();
        getSupportFragmentManager().beginTransaction().attach(tlf).commit();
    }

    @Override
    public void onDeleteClicked() {
        last = null;
        if(!twoPaneMode){
            getSupportFragmentManager().popBackStackImmediate();
        }
        TransactionListFragment tlf = (TransactionListFragment) getSupportFragmentManager().findFragmentById(R.id.transaction_list);
        Bundle argument = new Bundle();
        argument.putString("delete","delete");
        tlf.setArguments(argument);
        getSupportFragmentManager().beginTransaction().detach(tlf).commitNow();
        getSupportFragmentManager().beginTransaction().attach(tlf).commit();
    }

    @Override
    public void onDetailsClosedClicked(Bundle bundle) {
        last = null;
        if(!twoPaneMode){
            getSupportFragmentManager().popBackStackImmediate();
        }
        TransactionListFragment tlf = (TransactionListFragment) getSupportFragmentManager().findFragmentById(R.id.transaction_list);
        tlf.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().detach(tlf).commitNow();
        getSupportFragmentManager().beginTransaction().attach(tlf).commit();
        if(twoPaneMode){
            AddTransactionFragment detailFragment=null;
            detailFragment = new AddTransactionFragment();
            getSupportFragmentManager().beginTransaction().
                    replace(R.id.transaction_detail,detailFragment)
                    .commit();
        }
    }

    @Override
    public void onUndoClicked() {
        last = null;
        if(!twoPaneMode){
            getSupportFragmentManager().popBackStackImmediate();
        }
        TransactionListFragment tlf = (TransactionListFragment) getSupportFragmentManager().findFragmentById(R.id.transaction_list);
        Bundle argument = new Bundle();
        argument.putString("undo","undo");
        tlf.setArguments(argument);
        getSupportFragmentManager().beginTransaction().detach(tlf).commitNow();
        getSupportFragmentManager().beginTransaction().attach(tlf).commit();
    }

    @Override
    public void onBackPressed() {
        last = null;
        super.onBackPressed();
    }

    @Override
    public void swipeTop() {

    }

    @Override
    public void swipeBottom() {

    }

    @Override
    public void swipeRight() {
        try {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.transaction_list);
            if(fragment instanceof TransactionListFragment) {
                GraphsFragment gf = new GraphsFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.transaction_list,gf).addToBackStack(null).commit();
            }
            else if(fragment instanceof BudgetFragment){
                TransactionListFragment tlf = new TransactionListFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.transaction_list,tlf).addToBackStack(null).commit();
            }else if(fragment instanceof GraphsFragment){
                BudgetFragment bf = new BudgetFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.transaction_list, bf).addToBackStack(null).commit();
            }
        }catch (Exception e){

        }
    }

    @Override
    public void swipeLeft() {
        try {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.transaction_list);
            if(fragment instanceof TransactionListFragment) {
                BudgetFragment bf = new BudgetFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.transaction_list, bf).addToBackStack(null).commit();
            }
            else if(fragment instanceof BudgetFragment){
                GraphsFragment gf = new GraphsFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.transaction_list,gf).addToBackStack(null).commit();
            }else if(fragment instanceof GraphsFragment){
                TransactionListFragment tlf = new TransactionListFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.transaction_list,tlf).addToBackStack(null).commit();
            }
        }catch (Exception e){

        }
    }

    @Override
    public void onReceiveResult(int result, Bundle bundle) {
        switch (result){
            case 0: // SLUCAJ RUNNING

                break;
            case 1: // SLUCAJ USPJESNO

                break;
            case 2: // SLUCAJ ERROR

                break;
        }
    }
    @Override
    public void onResume() {

        super.onResume();
        registerReceiver(receiver, filter);
    }

    @Override
    public void onPause() {

        unregisterReceiver(receiver);
        super.onPause();
    }
}


