package com.example.spirala1.list;

import android.content.Context;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;

import com.example.spirala1.R;
import com.example.spirala1.data.Account;
import com.example.spirala1.data.Payment;
import com.example.spirala1.data.Transaction;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;

public class TLI extends AsyncTask<String, Integer, Void> {

    private String root = "http://rma20-app-rmaws.apps.us-west-1.starter.openshift-online.com";
    private String api_id = "60aa1f7c-f8d0-4a2e-9f8c-a4098e92302c";
    private ArrayList<Transaction> transactions;
    private Account account;
    private interactorInterface caller;
    private String command;

    public TLI(interactorInterface inter) {
        caller = inter;
        transactions = new ArrayList<>();
        account = null;
    }
    //KADA SE KORISTI KONSTRUKTOR BEZ PARAMETARA NE OBAVIJESTI SE DA BI TREBALO UPDATE LISTU
    public TLI(){
        caller = null;
        transactions = new ArrayList<>();
        account = null;
    }

    public interface interactorInterface{
        public void onDone(ArrayList<Transaction> results, Account account);
        public void saveCurrentState(ArrayList<Transaction> results, Account account);
    }

    private Integer getYearFromDateString(String s){
        if(s.equals("null"))return 0;
        String[] first = s.split(":");
        String[] second = first[0].split("-");
        return Integer.parseInt(second[0]);
    }
    private Integer getMonthFromDateString(String s){
        if(s.equals("null"))return 0;
        String[] first = s.split(":");
        String[] second = first[0].split("-");
        return Integer.parseInt(second[1]);
    }
    private Integer getDayFromDateString(String s){
        if(s.equals("null"))return 0;
        String[] first = s.split(":");
        String[] second = first[0].split("-");
        String[] third = second[2].split("T");
        return Integer.parseInt(third[0]);
    }
    private Payment getPaymentByID(int id){
        if(id > 5)return null;
        String querry = root+"/transactionTypes";
        try {
            URL url = new URL(querry);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            InputStream is = new BufferedInputStream(urlConnection.getInputStream());
            String result = convertStreamToString(is);
            JSONObject object = new JSONObject(result);
            JSONArray array = object.getJSONArray("rows");
            for(int i = 0; i < array.length(); i++){
                JSONObject type = array.getJSONObject(i);
                int trenutni = type.getInt("id");
                if(trenutni==id){
                    String name = type.getString("name");
                    switch (name){
                        case "Regular payment":
                            return Payment.REGULARPAYMENT;
                        case "Regular income":
                            return Payment.REGULARINCOME;
                        case "Purchase":
                            return Payment.PURCHASE;
                        case "Individual payment":
                            return Payment.INDIVIDUALPAYMENT;
                        case "Individual income":
                            return Payment.INDIVIDUALINCOME;
                        default:
                            return Payment.REGULARINCOME;
                    }
                }
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return Payment.REGULARINCOME;
    }

    private Transaction getTransactionFromJSON(JSONObject transaction){
        try{
            Integer day = getDayFromDateString(transaction.getString("date"));
            Integer month = getMonthFromDateString(transaction.getString("date"));
            Integer year = getYearFromDateString(transaction.getString("date"));
            String title = transaction.getString("title");
            Double amount = transaction.getDouble("amount");
            String description = transaction.getString("itemDescription");
            Payment type = getPaymentByID(transaction.getInt("TransactionTypeId"));
            String priv = transaction.getString("transactionInterval");
            Integer interval = 0;
            if(!priv.equals("null"))interval=transaction.getInt("transactionInterval");
            Integer eday = getDayFromDateString(transaction.getString("endDate"));
            Integer emonth = getMonthFromDateString(transaction.getString("endDate"));
            Integer eyear = getYearFromDateString(transaction.getString("endDate"));
            Integer id = transaction.getInt("id");
            return new Transaction(year,month,day,amount,title,type,description,interval,eyear,emonth,eday,id);
        }catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected Void doInBackground(String... strings) {
        transactions = new ArrayList<>();
        if(strings.length>0){
            command = strings[0];
            if(strings[0].equals("GETALL")){
                String querry = root + "/account/" + api_id + "/transactions?page=";
                Integer i = 0;
                while(true){
                    String link = querry+i.toString();
                    try {
                        URL url = new URL(link);
                        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                        urlConnection.setRequestMethod("GET");
                        InputStream is = new BufferedInputStream(urlConnection.getInputStream());
                        String result = convertStreamToString(is);
                        JSONObject jo = new JSONObject(result);
                        JSONArray results = jo.getJSONArray("transactions");
                        if(results.length()<1)break;
                        for(int j = 0; j < results.length(); j++){
                            transactions.add(getTransactionFromJSON(results.getJSONObject(j)));
                        }
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    i++;
                }
                querry = root + "/account/" + api_id;
                String link = querry;
                try {
                    URL url = new URL(link);
                    HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.setRequestMethod("GET");
                    InputStream is = new BufferedInputStream(urlConnection.getInputStream());
                    String result = convertStreamToString(is);
                    JSONObject jo = new JSONObject(result);
                    account = new Account();
                    account.setBalance(jo.getDouble("budget"));
                    account.setTotalLimit(jo.getDouble("totalLimit"));
                    account.setMonthLimit(jo.getDouble("monthLimit"));
                    urlConnection.disconnect();
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            // ORDER: 1-TYPE 2-MONTH 3-YEAR 4-SORT
            if(strings[0].equals("GETALLFILTER")){
                ArrayList<Transaction> priv = new ArrayList<>();
                String querry = root + "/account/" + api_id + "/transactions?page=";
                Integer i = 0;
                while(true){
                    String link = querry+i.toString();
                    try {
                        //link = URLEncoder.encode(link,"utf-8");
                        URL url = new URL(link);
                        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                        urlConnection.setRequestMethod("GET");
                        InputStream is = new BufferedInputStream(urlConnection.getInputStream());
                        String result = convertStreamToString(is);
                        JSONObject jo = new JSONObject(result);
                        JSONArray results = jo.getJSONArray("transactions");
                        if(results.length()<1)break;
                        for(int j = 0; j < results.length(); j++){
                            priv.add(getTransactionFromJSON(results.getJSONObject(j)));
                        }
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    i++;
                }
                for(Transaction t: priv){
                    if(!strings[1].equals("null") && !strings[1].equals("Filter by")){
                        if(!strings[2].equals("null") && !strings[3].equals("null") && strings[1].equals(t.getType().toString())){
                            Calendar c = Calendar.getInstance();
                            c.set(Integer.parseInt(strings[3]),Integer.parseInt(strings[2])-1,1,0,0,0);
                            int x = timesToShow(t, c);
                            if(x>0){
                                while(x>0){
                                    transactions.add(t);
                                    x--;
                                }
                            }
                            else if(sameMonthAndYear(c,t.getDate())){
                                transactions.add(t);
                            }
                        }
                        else if(strings[1].equals(t.getType().toString()))
                            transactions.add(t);
                    }
                    else if(!strings[2].equals("null") && !strings[3].equals("null")){
                        Calendar c = Calendar.getInstance();
                        c.set(Integer.parseInt(strings[3]),Integer.parseInt(strings[2])-1,1,0,0,0);
                        int x = timesToShow(t, c);
                        if(x>0){
                            while(x>0){
                                transactions.add(t);
                                x--;
                            }
                        }
                        else if(sameMonthAndYear(c,t.getDate())){
                            transactions.add(t);
                        }
                    }
                    if(!strings[4].equals("null")){
                        switch (strings[4]){
                            case "Price - Ascending":
                                Collections.sort(transactions,(t1, t2)->Double.compare(t1.getAmount(),t2.getAmount()));
                                break;
                            case "Price - Descending":
                                Collections.sort(transactions,(t1,t2)->Double.compare(t2.getAmount(),t1.getAmount()));
                                break;
                            case "Title - Ascending":
                                Collections.sort(transactions,(t1,t2)->t2.getTitle().compareTo(t1.getTitle()));
                                break;
                            case "Title - Descending":
                                Collections.sort(transactions,(t1,t2)->t1.getTitle().compareTo(t2.getTitle()));
                                break;
                            case "Date - Ascending":
                                Collections.sort(transactions,(t1,t2)->t1.getDate().compareTo(t2.getDate()));
                                break;
                            case "Date - Descending":
                                Collections.sort(transactions,(t1,t2)->t2.getDate().compareTo(t1.getDate()));
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
            // ORDER: 1-TYPE 2-MONTH 3-YEAR 4-SORT
            if(strings[0].equals("FILTER")){
                String querry = root + "/account/" + api_id + "/transactions/filter?page=";
                String suffix = "";
                if(strings[1]!=null){
                    suffix+="&typeId="+strings[1];
                }
                if(strings[2]!=null){
                    suffix+="&month="+strings[2];
                }
                if(strings[3]!=null){
                    suffix+="&year="+strings[3];
                }
                if(strings[4]!=null){
                    suffix+="&sort="+strings[4];
                }
                Integer i = 0;
                while(true){
                    String link = querry+i.toString()+suffix;
                    try {
                        URL url = new URL(link);
                        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                        urlConnection.setRequestMethod("GET");
                        InputStream is = new BufferedInputStream(urlConnection.getInputStream());
                        String result = convertStreamToString(is);
                        JSONObject jo = new JSONObject(result);
                        JSONArray results = jo.getJSONArray("transactions");
                        if(results.length()<1)break;
                        for(int j = 0; j < results.length(); j++){
                            transactions.add(getTransactionFromJSON(results.getJSONObject(j)));
                        }
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    i++;
                }
            }
            //ORDER 1-TITLE 2-AMOUNT 3-TYPE 4-DESCRIPTION 5-INTERVAL 6-DATE 7-ENDDATE
            if(strings[0].equals("ADD")){
                String querry = root + "/account/" + api_id + "/transactions";
                String body = getJsonTransaction(strings);
                try{
                    URL url = new URL(querry);
                    HttpURLConnection con = (HttpURLConnection)url.openConnection();
                    con.setRequestMethod("POST");
                    con.setRequestProperty("Content-Type", "application/json");
                    con.setRequestProperty("Accept", "application/json");
                    con.setDoOutput(true);
                    try(OutputStream os = con.getOutputStream()) {
                        byte[] input = body.getBytes("utf-8");
                        os.write(input, 0, input.length);
                    }
                    try(BufferedReader br = new BufferedReader(
                            new InputStreamReader(con.getInputStream(), "utf-8"))) {
                        StringBuilder response = new StringBuilder();
                        String responseLine = null;
                        while ((responseLine = br.readLine()) != null) {
                            response.append(responseLine.trim());
                        }
                        Log.d("RESPONSE", response.toString());
                    }
                }catch(Exception e){
                    e.printStackTrace();
                }
            }
            //ORDER LIKE BEFORE, 8-ID
            if(strings[0].equals("EDIT")){
                String querry = root + "/account/" + api_id + "/transactions/"+strings[8];
                String body = getJsonTransaction(strings);
                try{
                    URL url = new URL(querry);
                    HttpURLConnection con = (HttpURLConnection)url.openConnection();
                    con.setRequestMethod("POST");
                    con.setRequestProperty("Content-Type", "application/json");
                    con.setRequestProperty("Accept", "application/json");
                    con.setDoOutput(true);
                    try(OutputStream os = con.getOutputStream()) {
                        byte[] input = body.getBytes("utf-8");
                        os.write(input, 0, input.length);
                    }
                    try(BufferedReader br = new BufferedReader(
                            new InputStreamReader(con.getInputStream(), "utf-8"))) {
                        StringBuilder response = new StringBuilder();
                        String responseLine = null;
                        while ((responseLine = br.readLine()) != null) {
                            response.append(responseLine.trim());
                        }
                        Log.d("RESPONSE", response.toString());
                    }
                }catch(Exception e){
                    e.printStackTrace();
                }
            }
            //ORDER ID-1
            if(strings[0].equals("DELETE")){
                String querry = root + "/account/" + api_id + "/transactions/"+strings[1];
                try{
                    URL url = new URL(querry);
                    HttpURLConnection con = (HttpURLConnection)url.openConnection();
                    con.setRequestMethod("DELETE");
                    con.setRequestProperty("Accept", "application/json");
                    int result = con.getResponseCode();
                    Log.d("DELETE RESPONSE: ",result+"");
                }catch(Exception e){
                    e.printStackTrace();
                }
            }
            if(strings[0].equals("GETACC")){
                String querry = root + "/account/" + api_id;
                String link = querry;
                try {
                    URL url = new URL(link);
                    HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.setRequestMethod("GET");
                    InputStream is = new BufferedInputStream(urlConnection.getInputStream());
                    String result = convertStreamToString(is);
                    JSONObject jo = new JSONObject(result);
                    account = new Account();
                    account.setBalance(jo.getDouble("budget"));
                    account.setTotalLimit(jo.getDouble("totalLimit"));
                    account.setMonthLimit(jo.getDouble("monthLimit"));
                    urlConnection.disconnect();
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            if(strings[0].equals("EDITACC")){
                String querry = root + "/account/" + api_id;
                String body = getJsonAccount(strings);
                try{
                    URL url = new URL(querry);
                    HttpURLConnection con = (HttpURLConnection)url.openConnection();
                    con.setRequestMethod("POST");
                    con.setRequestProperty("Content-Type", "application/json");
                    con.setRequestProperty("Accept", "application/json");
                    con.setDoOutput(true);
                    try(OutputStream os = con.getOutputStream()) {
                        byte[] input = body.getBytes("utf-8");
                        os.write(input, 0, input.length);
                    }
                    try(BufferedReader br = new BufferedReader(
                            new InputStreamReader(con.getInputStream(), "utf-8"))) {
                        StringBuilder response = new StringBuilder();
                        String responseLine = null;
                        while ((responseLine = br.readLine()) != null) {
                            response.append(responseLine.trim());
                        }
                        Log.d("RESPONSE", response.toString());
                    }
                }catch(Exception e){
                    e.printStackTrace();
                }
                //DOHVATI EDITOVANI ACCOUNT
                querry = root + "/account/" + api_id;
                String link = querry;
                try {
                    URL url = new URL(link);
                    HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.setRequestMethod("GET");
                    InputStream is = new BufferedInputStream(urlConnection.getInputStream());
                    String result = convertStreamToString(is);
                    JSONObject jo = new JSONObject(result);
                    account = new Account();
                    account.setBalance(jo.getDouble("budget"));
                    account.setTotalLimit(jo.getDouble("totalLimit"));
                    account.setMonthLimit(jo.getDouble("monthLimit"));
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    private boolean sameMonthAndYear(Calendar a, Calendar b) {
        SimpleDateFormat month = new SimpleDateFormat("MMMM");
        SimpleDateFormat year = new SimpleDateFormat("YYYY");
        return month.format(a.getTime()).equals(month.format(b.getTime())) &&
                year.format(a.getTime()).equals(year.format(b.getTime()));
    }

    private int timesToShow(Transaction t, Calendar c) {
        if(!t.isRegularType()&&sameMonthAndYear(c,t.getDate()))return 1;
        else if(!t.isRegularType())return 0;
        if(t.getEndDate().before(c))return 0;
        Calendar tren = (Calendar) t.getDate().clone();
        do{
            tren.add(Calendar.DAY_OF_MONTH,t.getTransactionInterval());
            if(sameMonthAndYear(c,tren))break;
        }while(tren.before(c));
        int brojac = 0;
        while (sameMonthAndYear(c,tren)){
            brojac++;
            tren.add(Calendar.DAY_OF_MONTH,t.getTransactionInterval());
        }
        return brojac;
    }

    private String getJsonTransaction(String[] strings) {
        String[] order = {"\"title\": ","\"amount\": ","\"TransactionTypeId\": ", "\"itemDescription\": ","\"transactionInterval\": ", "\"date\": ", "\"endDate\": "};
        String vrati = "{ ";
        for(int i = 1; i < order.length+1; i++){
            if(strings[i]!=null){
                try{
                    //TEST JE LI BROJCANA VRIJEDNOST
                    Double.parseDouble(strings[i]);
                    vrati+=order[i-1]+strings[i]+", ";
                }catch (Exception e){
                    vrati += order[i-1]+"\""+strings[i]+"\", ";
                }
            }
        }
        vrati = vrati.substring(0,vrati.length()-2);
        vrati+="}";
        return vrati;
    }
    private String getJsonAccount(String[] strings){
        String[] order = {"\"budget\": ", "\"totalLimit\": ","\"monthLimit\": "};
        String vrati = "{ ";
        for(int i = 1; i < order.length+1; i++){
            if(!strings[i].equals("null")){
                vrati+=order[i-1]+strings[i]+", ";
            }
        }
        vrati = vrati.substring(0,vrati.length()-2);
        vrati+="}";
        return vrati;
    }

    private Integer getID(String string) {
        return 1;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        if(caller!=null && !command.equals("GETALL"))
        caller.onDone(transactions,account);
        if(caller != null && command.equals("GETALL")){
            caller.saveCurrentState(transactions,account);
        }
    }


    private String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
        } finally {
            try {
                is.close();
            } catch (IOException e) {
            }
        }
        return sb.toString();
    }
}
