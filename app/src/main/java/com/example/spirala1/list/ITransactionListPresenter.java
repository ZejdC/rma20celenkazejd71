package com.example.spirala1.list;

import android.os.Bundle;

import com.example.spirala1.data.Account;
import com.example.spirala1.data.Transaction;

import java.util.Calendar;
import java.util.List;

public interface ITransactionListPresenter {
    boolean sameMonthAndYear(Calendar a, Calendar b);
    public int timesBetweenStartAndEnd(Transaction t, Calendar end);
    int timesToShow(Transaction t);
    public void updateListToCurrentDate();
    void showTransactionsOnCurrentDate(List<Transaction> list);
    public void prepareToChange(Transaction t);
    public void savePrepared(Bundle data);
    public void deletePrepared();
    public void addToTransactions(Bundle data);
    public void filter(String[] str);
    public void getAccount();

    void addToTable(String add, Bundle arguments);

    void undo(Bundle arguments);
}
